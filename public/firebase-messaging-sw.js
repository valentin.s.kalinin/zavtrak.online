importScripts('https://www.gstatic.com/firebasejs/4.5.0/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/4.5.0/firebase-messaging.js');

var config = {
  apiKey: "AIzaSyDlOXxYNQ1wvShvQPL5USKQg89mZh9VAcU",
  authDomain: "zavtrak-online.firebaseapp.com",
  databaseURL: "https://zavtrak-online.firebaseio.com",
  projectId: "zavtrak-online",
  storageBucket: "zavtrak-online.appspot.com",
  messagingSenderId: "637396873160"

};

firebase.initializeApp(config);

const messaging = firebase.messaging();

// If you would like to customize notifications that are received in the
// background (Web app is closed or not in browser focus) then you should
// implement this optional method.
// [START background_handler]
messaging.setBackgroundMessageHandler(function(payload) {
  console.log('[firebase-messaging-sw.js] Received background message ', payload);
  // Customize notification here
  const notificationTitle = 'Background Message Title';
  const notificationOptions = {
    body: 'Background Message body.',
    icon: '/firebase-logo.png'
  };

  return self.registration.showNotification(notificationTitle,
      notificationOptions);
});
