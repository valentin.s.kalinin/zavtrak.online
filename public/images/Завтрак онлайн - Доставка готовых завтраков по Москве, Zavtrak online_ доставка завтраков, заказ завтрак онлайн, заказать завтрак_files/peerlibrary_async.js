//////////////////////////////////////////////////////////////////////////
//                                                                      //
// This is a generated file. You can view the original                  //
// source in your browser if your browser supports source maps.         //
// Source maps are supported by all recent versions of Chrome, Safari,  //
// and Firefox, and by Internet Explorer 11.                            //
//                                                                      //
//////////////////////////////////////////////////////////////////////////


(function () {

/* Imports */
var Meteor = Package.meteor.Meteor;
var global = Package.meteor.global;
var meteorEnv = Package.meteor.meteorEnv;

/* Package-scope variables */
var module, async;

(function(){

////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                        //
// packages/peerlibrary_async/packages/peerlibrary_async.js                                               //
//                                                                                                        //
////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                          //
(function () {

////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                //
// packages/peerlibrary:async/before.js                                                           //
//                                                                                                //
////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                  //
module = {                                                                                        // 1
  exports: {}                                                                                     // 2
};                                                                                                // 3
                                                                                                  // 4
////////////////////////////////////////////////////////////////////////////////////////////////////

}).call(this);






(function () {

////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                //
// packages/peerlibrary:async/async/lib/async.js                                                  //
//                                                                                                //
////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                  //
/*!                                                                                               // 1
 * async                                                                                          // 2
 * https://github.com/caolan/async                                                                // 3
 *                                                                                                // 4
 * Copyright 2010-2014 Caolan McMahon                                                             // 5
 * Released under the MIT license                                                                 // 6
 */                                                                                               // 7
(function () {                                                                                    // 8
                                                                                                  // 9
    var async = {};                                                                               // 10
    function noop() {}                                                                            // 11
    function identity(v) {                                                                        // 12
        return v;                                                                                 // 13
    }                                                                                             // 14
    function toBool(v) {                                                                          // 15
        return !!v;                                                                               // 16
    }                                                                                             // 17
    function notId(v) {                                                                           // 18
        return !v;                                                                                // 19
    }                                                                                             // 20
                                                                                                  // 21
    // global on the server, window in the browser                                                // 22
    var previous_async;                                                                           // 23
                                                                                                  // 24
    // Establish the root object, `window` (`self`) in the browser, `global`                      // 25
    // on the server, or `this` in some virtual machines. We use `self`                           // 26
    // instead of `window` for `WebWorker` support.                                               // 27
    var root = typeof self === 'object' && self.self === self && self ||                          // 28
            typeof global === 'object' && global.global === global && global ||                   // 29
            this;                                                                                 // 30
                                                                                                  // 31
    if (root != null) {                                                                           // 32
        previous_async = root.async;                                                              // 33
    }                                                                                             // 34
                                                                                                  // 35
    async.noConflict = function () {                                                              // 36
        root.async = previous_async;                                                              // 37
        return async;                                                                             // 38
    };                                                                                            // 39
                                                                                                  // 40
    function only_once(fn) {                                                                      // 41
        return function() {                                                                       // 42
            if (fn === null) throw new Error("Callback was already called.");                     // 43
            fn.apply(this, arguments);                                                            // 44
            fn = null;                                                                            // 45
        };                                                                                        // 46
    }                                                                                             // 47
                                                                                                  // 48
    function _once(fn) {                                                                          // 49
        return function() {                                                                       // 50
            if (fn === null) return;                                                              // 51
            fn.apply(this, arguments);                                                            // 52
            fn = null;                                                                            // 53
        };                                                                                        // 54
    }                                                                                             // 55
                                                                                                  // 56
    //// cross-browser compatiblity functions ////                                                // 57
                                                                                                  // 58
    var _toString = Object.prototype.toString;                                                    // 59
                                                                                                  // 60
    var _isArray = Array.isArray || function (obj) {                                              // 61
        return _toString.call(obj) === '[object Array]';                                          // 62
    };                                                                                            // 63
                                                                                                  // 64
    // Ported from underscore.js isObject                                                         // 65
    var _isObject = function(obj) {                                                               // 66
        var type = typeof obj;                                                                    // 67
        return type === 'function' || type === 'object' && !!obj;                                 // 68
    };                                                                                            // 69
                                                                                                  // 70
    function _isArrayLike(arr) {                                                                  // 71
        return _isArray(arr) || (                                                                 // 72
            // has a positive integer length property                                             // 73
            typeof arr.length === "number" &&                                                     // 74
            arr.length >= 0 &&                                                                    // 75
            arr.length % 1 === 0                                                                  // 76
        );                                                                                        // 77
    }                                                                                             // 78
                                                                                                  // 79
    function _arrayEach(arr, iterator) {                                                          // 80
        var index = -1,                                                                           // 81
            length = arr.length;                                                                  // 82
                                                                                                  // 83
        while (++index < length) {                                                                // 84
            iterator(arr[index], index, arr);                                                     // 85
        }                                                                                         // 86
    }                                                                                             // 87
                                                                                                  // 88
    function _map(arr, iterator) {                                                                // 89
        var index = -1,                                                                           // 90
            length = arr.length,                                                                  // 91
            result = Array(length);                                                               // 92
                                                                                                  // 93
        while (++index < length) {                                                                // 94
            result[index] = iterator(arr[index], index, arr);                                     // 95
        }                                                                                         // 96
        return result;                                                                            // 97
    }                                                                                             // 98
                                                                                                  // 99
    function _range(count) {                                                                      // 100
        return _map(Array(count), function (v, i) { return i; });                                 // 101
    }                                                                                             // 102
                                                                                                  // 103
    function _reduce(arr, iterator, memo) {                                                       // 104
        _arrayEach(arr, function (x, i, a) {                                                      // 105
            memo = iterator(memo, x, i, a);                                                       // 106
        });                                                                                       // 107
        return memo;                                                                              // 108
    }                                                                                             // 109
                                                                                                  // 110
    function _forEachOf(object, iterator) {                                                       // 111
        _arrayEach(_keys(object), function (key) {                                                // 112
            iterator(object[key], key);                                                           // 113
        });                                                                                       // 114
    }                                                                                             // 115
                                                                                                  // 116
    function _indexOf(arr, item) {                                                                // 117
        for (var i = 0; i < arr.length; i++) {                                                    // 118
            if (arr[i] === item) return i;                                                        // 119
        }                                                                                         // 120
        return -1;                                                                                // 121
    }                                                                                             // 122
                                                                                                  // 123
    var _keys = Object.keys || function (obj) {                                                   // 124
        var keys = [];                                                                            // 125
        for (var k in obj) {                                                                      // 126
            if (obj.hasOwnProperty(k)) {                                                          // 127
                keys.push(k);                                                                     // 128
            }                                                                                     // 129
        }                                                                                         // 130
        return keys;                                                                              // 131
    };                                                                                            // 132
                                                                                                  // 133
    function _keyIterator(coll) {                                                                 // 134
        var i = -1;                                                                               // 135
        var len;                                                                                  // 136
        var keys;                                                                                 // 137
        if (_isArrayLike(coll)) {                                                                 // 138
            len = coll.length;                                                                    // 139
            return function next() {                                                              // 140
                i++;                                                                              // 141
                return i < len ? i : null;                                                        // 142
            };                                                                                    // 143
        } else {                                                                                  // 144
            keys = _keys(coll);                                                                   // 145
            len = keys.length;                                                                    // 146
            return function next() {                                                              // 147
                i++;                                                                              // 148
                return i < len ? keys[i] : null;                                                  // 149
            };                                                                                    // 150
        }                                                                                         // 151
    }                                                                                             // 152
                                                                                                  // 153
    // Similar to ES6's rest param (http://ariya.ofilabs.com/2013/03/es6-and-rest-parameter.html) // 154
    // This accumulates the arguments passed into an array, after a given index.                  // 155
    // From underscore.js (https://github.com/jashkenas/underscore/pull/2140).                    // 156
    function _restParam(func, startIndex) {                                                       // 157
        startIndex = startIndex == null ? func.length - 1 : +startIndex;                          // 158
        return function() {                                                                       // 159
            var length = Math.max(arguments.length - startIndex, 0);                              // 160
            var rest = Array(length);                                                             // 161
            for (var index = 0; index < length; index++) {                                        // 162
                rest[index] = arguments[index + startIndex];                                      // 163
            }                                                                                     // 164
            switch (startIndex) {                                                                 // 165
                case 0: return func.call(this, rest);                                             // 166
                case 1: return func.call(this, arguments[0], rest);                               // 167
            }                                                                                     // 168
            // Currently unused but handle cases outside of the switch statement:                 // 169
            // var args = Array(startIndex + 1);                                                  // 170
            // for (index = 0; index < startIndex; index++) {                                     // 171
            //     args[index] = arguments[index];                                                // 172
            // }                                                                                  // 173
            // args[startIndex] = rest;                                                           // 174
            // return func.apply(this, args);                                                     // 175
        };                                                                                        // 176
    }                                                                                             // 177
                                                                                                  // 178
    function _withoutIndex(iterator) {                                                            // 179
        return function (value, index, callback) {                                                // 180
            return iterator(value, callback);                                                     // 181
        };                                                                                        // 182
    }                                                                                             // 183
                                                                                                  // 184
    //// exported async module functions ////                                                     // 185
                                                                                                  // 186
    //// nextTick implementation with browser-compatible fallback ////                            // 187
                                                                                                  // 188
    // capture the global reference to guard against fakeTimer mocks                              // 189
    var _setImmediate = typeof setImmediate === 'function' && setImmediate;                       // 190
                                                                                                  // 191
    var _delay = _setImmediate ? function(fn) {                                                   // 192
        // not a direct alias for IE10 compatibility                                              // 193
        _setImmediate(fn);                                                                        // 194
    } : function(fn) {                                                                            // 195
        setTimeout(fn, 0);                                                                        // 196
    };                                                                                            // 197
                                                                                                  // 198
    if (typeof process === 'object' && typeof process.nextTick === 'function') {                  // 199
        async.nextTick = process.nextTick;                                                        // 200
    } else {                                                                                      // 201
        async.nextTick = _delay;                                                                  // 202
    }                                                                                             // 203
    async.setImmediate = _setImmediate ? _delay : async.nextTick;                                 // 204
                                                                                                  // 205
                                                                                                  // 206
    async.forEach =                                                                               // 207
    async.each = function (arr, iterator, callback) {                                             // 208
        return async.eachOf(arr, _withoutIndex(iterator), callback);                              // 209
    };                                                                                            // 210
                                                                                                  // 211
    async.forEachSeries =                                                                         // 212
    async.eachSeries = function (arr, iterator, callback) {                                       // 213
        return async.eachOfSeries(arr, _withoutIndex(iterator), callback);                        // 214
    };                                                                                            // 215
                                                                                                  // 216
                                                                                                  // 217
    async.forEachLimit =                                                                          // 218
    async.eachLimit = function (arr, limit, iterator, callback) {                                 // 219
        return _eachOfLimit(limit)(arr, _withoutIndex(iterator), callback);                       // 220
    };                                                                                            // 221
                                                                                                  // 222
    async.forEachOf =                                                                             // 223
    async.eachOf = function (object, iterator, callback) {                                        // 224
        callback = _once(callback || noop);                                                       // 225
        object = object || [];                                                                    // 226
                                                                                                  // 227
        var iter = _keyIterator(object);                                                          // 228
        var key, completed = 0;                                                                   // 229
                                                                                                  // 230
        while ((key = iter()) != null) {                                                          // 231
            completed += 1;                                                                       // 232
            iterator(object[key], key, only_once(done));                                          // 233
        }                                                                                         // 234
                                                                                                  // 235
        if (completed === 0) callback(null);                                                      // 236
                                                                                                  // 237
        function done(err) {                                                                      // 238
            completed--;                                                                          // 239
            if (err) {                                                                            // 240
                callback(err);                                                                    // 241
            }                                                                                     // 242
            // Check key is null in case iterator isn't exhausted                                 // 243
            // and done resolved synchronously.                                                   // 244
            else if (key === null && completed <= 0) {                                            // 245
                callback(null);                                                                   // 246
            }                                                                                     // 247
        }                                                                                         // 248
    };                                                                                            // 249
                                                                                                  // 250
    async.forEachOfSeries =                                                                       // 251
    async.eachOfSeries = function (obj, iterator, callback) {                                     // 252
        callback = _once(callback || noop);                                                       // 253
        obj = obj || [];                                                                          // 254
        var nextKey = _keyIterator(obj);                                                          // 255
        var key = nextKey();                                                                      // 256
        function iterate() {                                                                      // 257
            var sync = true;                                                                      // 258
            if (key === null) {                                                                   // 259
                return callback(null);                                                            // 260
            }                                                                                     // 261
            iterator(obj[key], key, only_once(function (err) {                                    // 262
                if (err) {                                                                        // 263
                    callback(err);                                                                // 264
                }                                                                                 // 265
                else {                                                                            // 266
                    key = nextKey();                                                              // 267
                    if (key === null) {                                                           // 268
                        return callback(null);                                                    // 269
                    } else {                                                                      // 270
                        if (sync) {                                                               // 271
                            async.setImmediate(iterate);                                          // 272
                        } else {                                                                  // 273
                            iterate();                                                            // 274
                        }                                                                         // 275
                    }                                                                             // 276
                }                                                                                 // 277
            }));                                                                                  // 278
            sync = false;                                                                         // 279
        }                                                                                         // 280
        iterate();                                                                                // 281
    };                                                                                            // 282
                                                                                                  // 283
                                                                                                  // 284
                                                                                                  // 285
    async.forEachOfLimit =                                                                        // 286
    async.eachOfLimit = function (obj, limit, iterator, callback) {                               // 287
        _eachOfLimit(limit)(obj, iterator, callback);                                             // 288
    };                                                                                            // 289
                                                                                                  // 290
    function _eachOfLimit(limit) {                                                                // 291
                                                                                                  // 292
        return function (obj, iterator, callback) {                                               // 293
            callback = _once(callback || noop);                                                   // 294
            obj = obj || [];                                                                      // 295
            var nextKey = _keyIterator(obj);                                                      // 296
            if (limit <= 0) {                                                                     // 297
                return callback(null);                                                            // 298
            }                                                                                     // 299
            var done = false;                                                                     // 300
            var running = 0;                                                                      // 301
            var errored = false;                                                                  // 302
                                                                                                  // 303
            (function replenish () {                                                              // 304
                if (done && running <= 0) {                                                       // 305
                    return callback(null);                                                        // 306
                }                                                                                 // 307
                                                                                                  // 308
                while (running < limit && !errored) {                                             // 309
                    var key = nextKey();                                                          // 310
                    if (key === null) {                                                           // 311
                        done = true;                                                              // 312
                        if (running <= 0) {                                                       // 313
                            callback(null);                                                       // 314
                        }                                                                         // 315
                        return;                                                                   // 316
                    }                                                                             // 317
                    running += 1;                                                                 // 318
                    iterator(obj[key], key, only_once(function (err) {                            // 319
                        running -= 1;                                                             // 320
                        if (err) {                                                                // 321
                            callback(err);                                                        // 322
                            errored = true;                                                       // 323
                        }                                                                         // 324
                        else {                                                                    // 325
                            replenish();                                                          // 326
                        }                                                                         // 327
                    }));                                                                          // 328
                }                                                                                 // 329
            })();                                                                                 // 330
        };                                                                                        // 331
    }                                                                                             // 332
                                                                                                  // 333
                                                                                                  // 334
    function doParallel(fn) {                                                                     // 335
        return function (obj, iterator, callback) {                                               // 336
            return fn(async.eachOf, obj, iterator, callback);                                     // 337
        };                                                                                        // 338
    }                                                                                             // 339
    function doParallelLimit(fn) {                                                                // 340
        return function (obj, limit, iterator, callback) {                                        // 341
            return fn(_eachOfLimit(limit), obj, iterator, callback);                              // 342
        };                                                                                        // 343
    }                                                                                             // 344
    function doSeries(fn) {                                                                       // 345
        return function (obj, iterator, callback) {                                               // 346
            return fn(async.eachOfSeries, obj, iterator, callback);                               // 347
        };                                                                                        // 348
    }                                                                                             // 349
                                                                                                  // 350
    function _asyncMap(eachfn, arr, iterator, callback) {                                         // 351
        callback = _once(callback || noop);                                                       // 352
        arr = arr || [];                                                                          // 353
        var results = _isArrayLike(arr) ? [] : {};                                                // 354
        eachfn(arr, function (value, index, callback) {                                           // 355
            iterator(value, function (err, v) {                                                   // 356
                results[index] = v;                                                               // 357
                callback(err);                                                                    // 358
            });                                                                                   // 359
        }, function (err) {                                                                       // 360
            callback(err, results);                                                               // 361
        });                                                                                       // 362
    }                                                                                             // 363
                                                                                                  // 364
    async.map = doParallel(_asyncMap);                                                            // 365
    async.mapSeries = doSeries(_asyncMap);                                                        // 366
    async.mapLimit = doParallelLimit(_asyncMap);                                                  // 367
                                                                                                  // 368
    // reduce only has a series version, as doing reduce in parallel won't                        // 369
    // work in many situations.                                                                   // 370
    async.inject =                                                                                // 371
    async.foldl =                                                                                 // 372
    async.reduce = function (arr, memo, iterator, callback) {                                     // 373
        async.eachOfSeries(arr, function (x, i, callback) {                                       // 374
            iterator(memo, x, function (err, v) {                                                 // 375
                memo = v;                                                                         // 376
                callback(err);                                                                    // 377
            });                                                                                   // 378
        }, function (err) {                                                                       // 379
            callback(err, memo);                                                                  // 380
        });                                                                                       // 381
    };                                                                                            // 382
                                                                                                  // 383
    async.foldr =                                                                                 // 384
    async.reduceRight = function (arr, memo, iterator, callback) {                                // 385
        var reversed = _map(arr, identity).reverse();                                             // 386
        async.reduce(reversed, memo, iterator, callback);                                         // 387
    };                                                                                            // 388
                                                                                                  // 389
    async.transform = function (arr, memo, iterator, callback) {                                  // 390
        if (arguments.length === 3) {                                                             // 391
            callback = iterator;                                                                  // 392
            iterator = memo;                                                                      // 393
            memo = _isArray(arr) ? [] : {};                                                       // 394
        }                                                                                         // 395
                                                                                                  // 396
        async.eachOf(arr, function(v, k, cb) {                                                    // 397
            iterator(memo, v, k, cb);                                                             // 398
        }, function(err) {                                                                        // 399
            callback(err, memo);                                                                  // 400
        });                                                                                       // 401
    };                                                                                            // 402
                                                                                                  // 403
    function _filter(eachfn, arr, iterator, callback) {                                           // 404
        var results = [];                                                                         // 405
        eachfn(arr, function (x, index, callback) {                                               // 406
            iterator(x, function (v) {                                                            // 407
                if (v) {                                                                          // 408
                    results.push({index: index, value: x});                                       // 409
                }                                                                                 // 410
                callback();                                                                       // 411
            });                                                                                   // 412
        }, function () {                                                                          // 413
            callback(_map(results.sort(function (a, b) {                                          // 414
                return a.index - b.index;                                                         // 415
            }), function (x) {                                                                    // 416
                return x.value;                                                                   // 417
            }));                                                                                  // 418
        });                                                                                       // 419
    }                                                                                             // 420
                                                                                                  // 421
    async.select =                                                                                // 422
    async.filter = doParallel(_filter);                                                           // 423
                                                                                                  // 424
    async.selectLimit =                                                                           // 425
    async.filterLimit = doParallelLimit(_filter);                                                 // 426
                                                                                                  // 427
    async.selectSeries =                                                                          // 428
    async.filterSeries = doSeries(_filter);                                                       // 429
                                                                                                  // 430
    function _reject(eachfn, arr, iterator, callback) {                                           // 431
        _filter(eachfn, arr, function(value, cb) {                                                // 432
            iterator(value, function(v) {                                                         // 433
                cb(!v);                                                                           // 434
            });                                                                                   // 435
        }, callback);                                                                             // 436
    }                                                                                             // 437
    async.reject = doParallel(_reject);                                                           // 438
    async.rejectLimit = doParallelLimit(_reject);                                                 // 439
    async.rejectSeries = doSeries(_reject);                                                       // 440
                                                                                                  // 441
    function _createTester(eachfn, check, getResult) {                                            // 442
        return function(arr, limit, iterator, cb) {                                               // 443
            function done() {                                                                     // 444
                if (cb) cb(getResult(false, void 0));                                             // 445
            }                                                                                     // 446
            function iteratee(x, _, callback) {                                                   // 447
                if (!cb) return callback();                                                       // 448
                iterator(x, function (v) {                                                        // 449
                    if (cb && check(v)) {                                                         // 450
                        cb(getResult(true, x));                                                   // 451
                        cb = iterator = false;                                                    // 452
                    }                                                                             // 453
                    callback();                                                                   // 454
                });                                                                               // 455
            }                                                                                     // 456
            if (arguments.length > 3) {                                                           // 457
                eachfn(arr, limit, iteratee, done);                                               // 458
            } else {                                                                              // 459
                cb = iterator;                                                                    // 460
                iterator = limit;                                                                 // 461
                eachfn(arr, iteratee, done);                                                      // 462
            }                                                                                     // 463
        };                                                                                        // 464
    }                                                                                             // 465
                                                                                                  // 466
    async.any =                                                                                   // 467
    async.some = _createTester(async.eachOf, toBool, identity);                                   // 468
                                                                                                  // 469
    async.someLimit = _createTester(async.eachOfLimit, toBool, identity);                         // 470
                                                                                                  // 471
    async.all =                                                                                   // 472
    async.every = _createTester(async.eachOf, notId, notId);                                      // 473
                                                                                                  // 474
    async.everyLimit = _createTester(async.eachOfLimit, notId, notId);                            // 475
                                                                                                  // 476
    function _findGetResult(v, x) {                                                               // 477
        return x;                                                                                 // 478
    }                                                                                             // 479
    async.detect = _createTester(async.eachOf, identity, _findGetResult);                         // 480
    async.detectSeries = _createTester(async.eachOfSeries, identity, _findGetResult);             // 481
    async.detectLimit = _createTester(async.eachOfLimit, identity, _findGetResult);               // 482
                                                                                                  // 483
    async.sortBy = function (arr, iterator, callback) {                                           // 484
        async.map(arr, function (x, callback) {                                                   // 485
            iterator(x, function (err, criteria) {                                                // 486
                if (err) {                                                                        // 487
                    callback(err);                                                                // 488
                }                                                                                 // 489
                else {                                                                            // 490
                    callback(null, {value: x, criteria: criteria});                               // 491
                }                                                                                 // 492
            });                                                                                   // 493
        }, function (err, results) {                                                              // 494
            if (err) {                                                                            // 495
                return callback(err);                                                             // 496
            }                                                                                     // 497
            else {                                                                                // 498
                callback(null, _map(results.sort(comparator), function (x) {                      // 499
                    return x.value;                                                               // 500
                }));                                                                              // 501
            }                                                                                     // 502
                                                                                                  // 503
        });                                                                                       // 504
                                                                                                  // 505
        function comparator(left, right) {                                                        // 506
            var a = left.criteria, b = right.criteria;                                            // 507
            return a < b ? -1 : a > b ? 1 : 0;                                                    // 508
        }                                                                                         // 509
    };                                                                                            // 510
                                                                                                  // 511
    async.auto = function (tasks, concurrency, callback) {                                        // 512
        if (typeof arguments[1] === 'function') {                                                 // 513
            // concurrency is optional, shift the args.                                           // 514
            callback = concurrency;                                                               // 515
            concurrency = null;                                                                   // 516
        }                                                                                         // 517
        callback = _once(callback || noop);                                                       // 518
        var keys = _keys(tasks);                                                                  // 519
        var remainingTasks = keys.length;                                                         // 520
        if (!remainingTasks) {                                                                    // 521
            return callback(null);                                                                // 522
        }                                                                                         // 523
        if (!concurrency) {                                                                       // 524
            concurrency = remainingTasks;                                                         // 525
        }                                                                                         // 526
                                                                                                  // 527
        var results = {};                                                                         // 528
        var runningTasks = 0;                                                                     // 529
                                                                                                  // 530
        var hasError = false;                                                                     // 531
                                                                                                  // 532
        var listeners = [];                                                                       // 533
        function addListener(fn) {                                                                // 534
            listeners.unshift(fn);                                                                // 535
        }                                                                                         // 536
        function removeListener(fn) {                                                             // 537
            var idx = _indexOf(listeners, fn);                                                    // 538
            if (idx >= 0) listeners.splice(idx, 1);                                               // 539
        }                                                                                         // 540
        function taskComplete() {                                                                 // 541
            remainingTasks--;                                                                     // 542
            _arrayEach(listeners.slice(0), function (fn) {                                        // 543
                fn();                                                                             // 544
            });                                                                                   // 545
        }                                                                                         // 546
                                                                                                  // 547
        addListener(function () {                                                                 // 548
            if (!remainingTasks) {                                                                // 549
                callback(null, results);                                                          // 550
            }                                                                                     // 551
        });                                                                                       // 552
                                                                                                  // 553
        _arrayEach(keys, function (k) {                                                           // 554
            if (hasError) return;                                                                 // 555
            var task = _isArray(tasks[k]) ? tasks[k]: [tasks[k]];                                 // 556
            var taskCallback = _restParam(function(err, args) {                                   // 557
                runningTasks--;                                                                   // 558
                if (args.length <= 1) {                                                           // 559
                    args = args[0];                                                               // 560
                }                                                                                 // 561
                if (err) {                                                                        // 562
                    var safeResults = {};                                                         // 563
                    _forEachOf(results, function(val, rkey) {                                     // 564
                        safeResults[rkey] = val;                                                  // 565
                    });                                                                           // 566
                    safeResults[k] = args;                                                        // 567
                    hasError = true;                                                              // 568
                                                                                                  // 569
                    callback(err, safeResults);                                                   // 570
                }                                                                                 // 571
                else {                                                                            // 572
                    results[k] = args;                                                            // 573
                    async.setImmediate(taskComplete);                                             // 574
                }                                                                                 // 575
            });                                                                                   // 576
            var requires = task.slice(0, task.length - 1);                                        // 577
            // prevent dead-locks                                                                 // 578
            var len = requires.length;                                                            // 579
            var dep;                                                                              // 580
            while (len--) {                                                                       // 581
                if (!(dep = tasks[requires[len]])) {                                              // 582
                    throw new Error('Has nonexistent dependency in ' + requires.join(', '));      // 583
                }                                                                                 // 584
                if (_isArray(dep) && _indexOf(dep, k) >= 0) {                                     // 585
                    throw new Error('Has cyclic dependencies');                                   // 586
                }                                                                                 // 587
            }                                                                                     // 588
            function ready() {                                                                    // 589
                return runningTasks < concurrency && _reduce(requires, function (a, x) {          // 590
                    return (a && results.hasOwnProperty(x));                                      // 591
                }, true) && !results.hasOwnProperty(k);                                           // 592
            }                                                                                     // 593
            if (ready()) {                                                                        // 594
                runningTasks++;                                                                   // 595
                task[task.length - 1](taskCallback, results);                                     // 596
            }                                                                                     // 597
            else {                                                                                // 598
                addListener(listener);                                                            // 599
            }                                                                                     // 600
            function listener() {                                                                 // 601
                if (ready()) {                                                                    // 602
                    runningTasks++;                                                               // 603
                    removeListener(listener);                                                     // 604
                    task[task.length - 1](taskCallback, results);                                 // 605
                }                                                                                 // 606
            }                                                                                     // 607
        });                                                                                       // 608
    };                                                                                            // 609
                                                                                                  // 610
                                                                                                  // 611
                                                                                                  // 612
    async.retry = function(times, task, callback) {                                               // 613
        var DEFAULT_TIMES = 5;                                                                    // 614
        var DEFAULT_INTERVAL = 0;                                                                 // 615
                                                                                                  // 616
        var attempts = [];                                                                        // 617
                                                                                                  // 618
        var opts = {                                                                              // 619
            times: DEFAULT_TIMES,                                                                 // 620
            interval: DEFAULT_INTERVAL                                                            // 621
        };                                                                                        // 622
                                                                                                  // 623
        function parseTimes(acc, t){                                                              // 624
            if(typeof t === 'number'){                                                            // 625
                acc.times = parseInt(t, 10) || DEFAULT_TIMES;                                     // 626
            } else if(typeof t === 'object'){                                                     // 627
                acc.times = parseInt(t.times, 10) || DEFAULT_TIMES;                               // 628
                acc.interval = parseInt(t.interval, 10) || DEFAULT_INTERVAL;                      // 629
            } else {                                                                              // 630
                throw new Error('Unsupported argument type for \'times\': ' + typeof t);          // 631
            }                                                                                     // 632
        }                                                                                         // 633
                                                                                                  // 634
        var length = arguments.length;                                                            // 635
        if (length < 1 || length > 3) {                                                           // 636
            throw new Error('Invalid arguments - must be either (task), (task, callback), (times, task) or (times, task, callback)');
        } else if (length <= 2 && typeof times === 'function') {                                  // 638
            callback = task;                                                                      // 639
            task = times;                                                                         // 640
        }                                                                                         // 641
        if (typeof times !== 'function') {                                                        // 642
            parseTimes(opts, times);                                                              // 643
        }                                                                                         // 644
        opts.callback = callback;                                                                 // 645
        opts.task = task;                                                                         // 646
                                                                                                  // 647
        function wrappedTask(wrappedCallback, wrappedResults) {                                   // 648
            function retryAttempt(task, finalAttempt) {                                           // 649
                return function(seriesCallback) {                                                 // 650
                    task(function(err, result){                                                   // 651
                        seriesCallback(!err || finalAttempt, {err: err, result: result});         // 652
                    }, wrappedResults);                                                           // 653
                };                                                                                // 654
            }                                                                                     // 655
                                                                                                  // 656
            function retryInterval(interval){                                                     // 657
                return function(seriesCallback){                                                  // 658
                    setTimeout(function(){                                                        // 659
                        seriesCallback(null);                                                     // 660
                    }, interval);                                                                 // 661
                };                                                                                // 662
            }                                                                                     // 663
                                                                                                  // 664
            while (opts.times) {                                                                  // 665
                                                                                                  // 666
                var finalAttempt = !(opts.times-=1);                                              // 667
                attempts.push(retryAttempt(opts.task, finalAttempt));                             // 668
                if(!finalAttempt && opts.interval > 0){                                           // 669
                    attempts.push(retryInterval(opts.interval));                                  // 670
                }                                                                                 // 671
            }                                                                                     // 672
                                                                                                  // 673
            async.series(attempts, function(done, data){                                          // 674
                data = data[data.length - 1];                                                     // 675
                (wrappedCallback || opts.callback)(data.err, data.result);                        // 676
            });                                                                                   // 677
        }                                                                                         // 678
                                                                                                  // 679
        // If a callback is passed, run this as a controll flow                                   // 680
        return opts.callback ? wrappedTask() : wrappedTask;                                       // 681
    };                                                                                            // 682
                                                                                                  // 683
    async.waterfall = function (tasks, callback) {                                                // 684
        callback = _once(callback || noop);                                                       // 685
        if (!_isArray(tasks)) {                                                                   // 686
            var err = new Error('First argument to waterfall must be an array of functions');     // 687
            return callback(err);                                                                 // 688
        }                                                                                         // 689
        if (!tasks.length) {                                                                      // 690
            return callback();                                                                    // 691
        }                                                                                         // 692
        function wrapIterator(iterator) {                                                         // 693
            return _restParam(function (err, args) {                                              // 694
                if (err) {                                                                        // 695
                    callback.apply(null, [err].concat(args));                                     // 696
                }                                                                                 // 697
                else {                                                                            // 698
                    var next = iterator.next();                                                   // 699
                    if (next) {                                                                   // 700
                        args.push(wrapIterator(next));                                            // 701
                    }                                                                             // 702
                    else {                                                                        // 703
                        args.push(callback);                                                      // 704
                    }                                                                             // 705
                    ensureAsync(iterator).apply(null, args);                                      // 706
                }                                                                                 // 707
            });                                                                                   // 708
        }                                                                                         // 709
        wrapIterator(async.iterator(tasks))();                                                    // 710
    };                                                                                            // 711
                                                                                                  // 712
    function _parallel(eachfn, tasks, callback) {                                                 // 713
        callback = callback || noop;                                                              // 714
        var results = _isArrayLike(tasks) ? [] : {};                                              // 715
                                                                                                  // 716
        eachfn(tasks, function (task, key, callback) {                                            // 717
            task(_restParam(function (err, args) {                                                // 718
                if (args.length <= 1) {                                                           // 719
                    args = args[0];                                                               // 720
                }                                                                                 // 721
                results[key] = args;                                                              // 722
                callback(err);                                                                    // 723
            }));                                                                                  // 724
        }, function (err) {                                                                       // 725
            callback(err, results);                                                               // 726
        });                                                                                       // 727
    }                                                                                             // 728
                                                                                                  // 729
    async.parallel = function (tasks, callback) {                                                 // 730
        _parallel(async.eachOf, tasks, callback);                                                 // 731
    };                                                                                            // 732
                                                                                                  // 733
    async.parallelLimit = function(tasks, limit, callback) {                                      // 734
        _parallel(_eachOfLimit(limit), tasks, callback);                                          // 735
    };                                                                                            // 736
                                                                                                  // 737
    async.series = function(tasks, callback) {                                                    // 738
        _parallel(async.eachOfSeries, tasks, callback);                                           // 739
    };                                                                                            // 740
                                                                                                  // 741
    async.iterator = function (tasks) {                                                           // 742
        function makeCallback(index) {                                                            // 743
            function fn() {                                                                       // 744
                if (tasks.length) {                                                               // 745
                    tasks[index].apply(null, arguments);                                          // 746
                }                                                                                 // 747
                return fn.next();                                                                 // 748
            }                                                                                     // 749
            fn.next = function () {                                                               // 750
                return (index < tasks.length - 1) ? makeCallback(index + 1): null;                // 751
            };                                                                                    // 752
            return fn;                                                                            // 753
        }                                                                                         // 754
        return makeCallback(0);                                                                   // 755
    };                                                                                            // 756
                                                                                                  // 757
    async.apply = _restParam(function (fn, args) {                                                // 758
        return _restParam(function (callArgs) {                                                   // 759
            return fn.apply(                                                                      // 760
                null, args.concat(callArgs)                                                       // 761
            );                                                                                    // 762
        });                                                                                       // 763
    });                                                                                           // 764
                                                                                                  // 765
    function _concat(eachfn, arr, fn, callback) {                                                 // 766
        var result = [];                                                                          // 767
        eachfn(arr, function (x, index, cb) {                                                     // 768
            fn(x, function (err, y) {                                                             // 769
                result = result.concat(y || []);                                                  // 770
                cb(err);                                                                          // 771
            });                                                                                   // 772
        }, function (err) {                                                                       // 773
            callback(err, result);                                                                // 774
        });                                                                                       // 775
    }                                                                                             // 776
    async.concat = doParallel(_concat);                                                           // 777
    async.concatSeries = doSeries(_concat);                                                       // 778
                                                                                                  // 779
    async.whilst = function (test, iterator, callback) {                                          // 780
        callback = callback || noop;                                                              // 781
        if (test()) {                                                                             // 782
            var next = _restParam(function(err, args) {                                           // 783
                if (err) {                                                                        // 784
                    callback(err);                                                                // 785
                } else if (test.apply(this, args)) {                                              // 786
                    iterator(next);                                                               // 787
                } else {                                                                          // 788
                    callback.apply(null, [null].concat(args));                                    // 789
                }                                                                                 // 790
            });                                                                                   // 791
            iterator(next);                                                                       // 792
        } else {                                                                                  // 793
            callback(null);                                                                       // 794
        }                                                                                         // 795
    };                                                                                            // 796
                                                                                                  // 797
    async.doWhilst = function (iterator, test, callback) {                                        // 798
        var calls = 0;                                                                            // 799
        return async.whilst(function() {                                                          // 800
            return ++calls <= 1 || test.apply(this, arguments);                                   // 801
        }, iterator, callback);                                                                   // 802
    };                                                                                            // 803
                                                                                                  // 804
    async.until = function (test, iterator, callback) {                                           // 805
        return async.whilst(function() {                                                          // 806
            return !test.apply(this, arguments);                                                  // 807
        }, iterator, callback);                                                                   // 808
    };                                                                                            // 809
                                                                                                  // 810
    async.doUntil = function (iterator, test, callback) {                                         // 811
        return async.doWhilst(iterator, function() {                                              // 812
            return !test.apply(this, arguments);                                                  // 813
        }, callback);                                                                             // 814
    };                                                                                            // 815
                                                                                                  // 816
    async.during = function (test, iterator, callback) {                                          // 817
        callback = callback || noop;                                                              // 818
                                                                                                  // 819
        var next = _restParam(function(err, args) {                                               // 820
            if (err) {                                                                            // 821
                callback(err);                                                                    // 822
            } else {                                                                              // 823
                args.push(check);                                                                 // 824
                test.apply(this, args);                                                           // 825
            }                                                                                     // 826
        });                                                                                       // 827
                                                                                                  // 828
        var check = function(err, truth) {                                                        // 829
            if (err) {                                                                            // 830
                callback(err);                                                                    // 831
            } else if (truth) {                                                                   // 832
                iterator(next);                                                                   // 833
            } else {                                                                              // 834
                callback(null);                                                                   // 835
            }                                                                                     // 836
        };                                                                                        // 837
                                                                                                  // 838
        test(check);                                                                              // 839
    };                                                                                            // 840
                                                                                                  // 841
    async.doDuring = function (iterator, test, callback) {                                        // 842
        var calls = 0;                                                                            // 843
        async.during(function(next) {                                                             // 844
            if (calls++ < 1) {                                                                    // 845
                next(null, true);                                                                 // 846
            } else {                                                                              // 847
                test.apply(this, arguments);                                                      // 848
            }                                                                                     // 849
        }, iterator, callback);                                                                   // 850
    };                                                                                            // 851
                                                                                                  // 852
    function _queue(worker, concurrency, payload) {                                               // 853
        if (concurrency == null) {                                                                // 854
            concurrency = 1;                                                                      // 855
        }                                                                                         // 856
        else if(concurrency === 0) {                                                              // 857
            throw new Error('Concurrency must not be zero');                                      // 858
        }                                                                                         // 859
        function _insert(q, data, pos, callback) {                                                // 860
            if (callback != null && typeof callback !== "function") {                             // 861
                throw new Error("task callback must be a function");                              // 862
            }                                                                                     // 863
            q.started = true;                                                                     // 864
            if (!_isArray(data)) {                                                                // 865
                data = [data];                                                                    // 866
            }                                                                                     // 867
            if(data.length === 0 && q.idle()) {                                                   // 868
                // call drain immediately if there are no tasks                                   // 869
                return async.setImmediate(function() {                                            // 870
                    q.drain();                                                                    // 871
                });                                                                               // 872
            }                                                                                     // 873
            _arrayEach(data, function(task) {                                                     // 874
                var item = {                                                                      // 875
                    data: task,                                                                   // 876
                    callback: callback || noop                                                    // 877
                };                                                                                // 878
                                                                                                  // 879
                if (pos) {                                                                        // 880
                    q.tasks.unshift(item);                                                        // 881
                } else {                                                                          // 882
                    q.tasks.push(item);                                                           // 883
                }                                                                                 // 884
                                                                                                  // 885
                if (q.tasks.length === q.concurrency) {                                           // 886
                    q.saturated();                                                                // 887
                }                                                                                 // 888
            });                                                                                   // 889
            async.setImmediate(q.process);                                                        // 890
        }                                                                                         // 891
        function _next(q, tasks) {                                                                // 892
            return function(){                                                                    // 893
                workers -= 1;                                                                     // 894
                                                                                                  // 895
                var removed = false;                                                              // 896
                var args = arguments;                                                             // 897
                _arrayEach(tasks, function (task) {                                               // 898
                    _arrayEach(workersList, function (worker, index) {                            // 899
                        if (worker === task && !removed) {                                        // 900
                            workersList.splice(index, 1);                                         // 901
                            removed = true;                                                       // 902
                        }                                                                         // 903
                    });                                                                           // 904
                                                                                                  // 905
                    task.callback.apply(task, args);                                              // 906
                });                                                                               // 907
                if (q.tasks.length + workers === 0) {                                             // 908
                    q.drain();                                                                    // 909
                }                                                                                 // 910
                q.process();                                                                      // 911
            };                                                                                    // 912
        }                                                                                         // 913
                                                                                                  // 914
        var workers = 0;                                                                          // 915
        var workersList = [];                                                                     // 916
        var q = {                                                                                 // 917
            tasks: [],                                                                            // 918
            concurrency: concurrency,                                                             // 919
            payload: payload,                                                                     // 920
            saturated: noop,                                                                      // 921
            empty: noop,                                                                          // 922
            drain: noop,                                                                          // 923
            started: false,                                                                       // 924
            paused: false,                                                                        // 925
            push: function (data, callback) {                                                     // 926
                _insert(q, data, false, callback);                                                // 927
            },                                                                                    // 928
            kill: function () {                                                                   // 929
                q.drain = noop;                                                                   // 930
                q.tasks = [];                                                                     // 931
            },                                                                                    // 932
            unshift: function (data, callback) {                                                  // 933
                _insert(q, data, true, callback);                                                 // 934
            },                                                                                    // 935
            process: function () {                                                                // 936
                while(!q.paused && workers < q.concurrency && q.tasks.length){                    // 937
                                                                                                  // 938
                    var tasks = q.payload ?                                                       // 939
                        q.tasks.splice(0, q.payload) :                                            // 940
                        q.tasks.splice(0, q.tasks.length);                                        // 941
                                                                                                  // 942
                    var data = _map(tasks, function (task) {                                      // 943
                        return task.data;                                                         // 944
                    });                                                                           // 945
                                                                                                  // 946
                    if (q.tasks.length === 0) {                                                   // 947
                        q.empty();                                                                // 948
                    }                                                                             // 949
                    workers += 1;                                                                 // 950
                    workersList.push(tasks[0]);                                                   // 951
                    var cb = only_once(_next(q, tasks));                                          // 952
                    worker(data, cb);                                                             // 953
                }                                                                                 // 954
            },                                                                                    // 955
            length: function () {                                                                 // 956
                return q.tasks.length;                                                            // 957
            },                                                                                    // 958
            running: function () {                                                                // 959
                return workers;                                                                   // 960
            },                                                                                    // 961
            workersList: function () {                                                            // 962
                return workersList;                                                               // 963
            },                                                                                    // 964
            idle: function() {                                                                    // 965
                return q.tasks.length + workers === 0;                                            // 966
            },                                                                                    // 967
            pause: function () {                                                                  // 968
                q.paused = true;                                                                  // 969
            },                                                                                    // 970
            resume: function () {                                                                 // 971
                if (q.paused === false) { return; }                                               // 972
                q.paused = false;                                                                 // 973
                var resumeCount = Math.min(q.concurrency, q.tasks.length);                        // 974
                // Need to call q.process once per concurrent                                     // 975
                // worker to preserve full concurrency after pause                                // 976
                for (var w = 1; w <= resumeCount; w++) {                                          // 977
                    async.setImmediate(q.process);                                                // 978
                }                                                                                 // 979
            }                                                                                     // 980
        };                                                                                        // 981
        return q;                                                                                 // 982
    }                                                                                             // 983
                                                                                                  // 984
    async.queue = function (worker, concurrency) {                                                // 985
        var q = _queue(function (items, cb) {                                                     // 986
            worker(items[0], cb);                                                                 // 987
        }, concurrency, 1);                                                                       // 988
                                                                                                  // 989
        return q;                                                                                 // 990
    };                                                                                            // 991
                                                                                                  // 992
    async.priorityQueue = function (worker, concurrency) {                                        // 993
                                                                                                  // 994
        function _compareTasks(a, b){                                                             // 995
            return a.priority - b.priority;                                                       // 996
        }                                                                                         // 997
                                                                                                  // 998
        function _binarySearch(sequence, item, compare) {                                         // 999
            var beg = -1,                                                                         // 1000
                end = sequence.length - 1;                                                        // 1001
            while (beg < end) {                                                                   // 1002
                var mid = beg + ((end - beg + 1) >>> 1);                                          // 1003
                if (compare(item, sequence[mid]) >= 0) {                                          // 1004
                    beg = mid;                                                                    // 1005
                } else {                                                                          // 1006
                    end = mid - 1;                                                                // 1007
                }                                                                                 // 1008
            }                                                                                     // 1009
            return beg;                                                                           // 1010
        }                                                                                         // 1011
                                                                                                  // 1012
        function _insert(q, data, priority, callback) {                                           // 1013
            if (callback != null && typeof callback !== "function") {                             // 1014
                throw new Error("task callback must be a function");                              // 1015
            }                                                                                     // 1016
            q.started = true;                                                                     // 1017
            if (!_isArray(data)) {                                                                // 1018
                data = [data];                                                                    // 1019
            }                                                                                     // 1020
            if(data.length === 0) {                                                               // 1021
                // call drain immediately if there are no tasks                                   // 1022
                return async.setImmediate(function() {                                            // 1023
                    q.drain();                                                                    // 1024
                });                                                                               // 1025
            }                                                                                     // 1026
            _arrayEach(data, function(task) {                                                     // 1027
                var item = {                                                                      // 1028
                    data: task,                                                                   // 1029
                    priority: priority,                                                           // 1030
                    callback: typeof callback === 'function' ? callback : noop                    // 1031
                };                                                                                // 1032
                                                                                                  // 1033
                q.tasks.splice(_binarySearch(q.tasks, item, _compareTasks) + 1, 0, item);         // 1034
                                                                                                  // 1035
                if (q.tasks.length === q.concurrency) {                                           // 1036
                    q.saturated();                                                                // 1037
                }                                                                                 // 1038
                async.setImmediate(q.process);                                                    // 1039
            });                                                                                   // 1040
        }                                                                                         // 1041
                                                                                                  // 1042
        // Start with a normal queue                                                              // 1043
        var q = async.queue(worker, concurrency);                                                 // 1044
                                                                                                  // 1045
        // Override push to accept second parameter representing priority                         // 1046
        q.push = function (data, priority, callback) {                                            // 1047
            _insert(q, data, priority, callback);                                                 // 1048
        };                                                                                        // 1049
                                                                                                  // 1050
        // Remove unshift function                                                                // 1051
        delete q.unshift;                                                                         // 1052
                                                                                                  // 1053
        return q;                                                                                 // 1054
    };                                                                                            // 1055
                                                                                                  // 1056
    async.cargo = function (worker, payload) {                                                    // 1057
        return _queue(worker, 1, payload);                                                        // 1058
    };                                                                                            // 1059
                                                                                                  // 1060
    function _console_fn(name) {                                                                  // 1061
        return _restParam(function (fn, args) {                                                   // 1062
            fn.apply(null, args.concat([_restParam(function (err, args) {                         // 1063
                if (typeof console === 'object') {                                                // 1064
                    if (err) {                                                                    // 1065
                        if (console.error) {                                                      // 1066
                            console.error(err);                                                   // 1067
                        }                                                                         // 1068
                    }                                                                             // 1069
                    else if (console[name]) {                                                     // 1070
                        _arrayEach(args, function (x) {                                           // 1071
                            console[name](x);                                                     // 1072
                        });                                                                       // 1073
                    }                                                                             // 1074
                }                                                                                 // 1075
            })]));                                                                                // 1076
        });                                                                                       // 1077
    }                                                                                             // 1078
    async.log = _console_fn('log');                                                               // 1079
    async.dir = _console_fn('dir');                                                               // 1080
    /*async.info = _console_fn('info');                                                           // 1081
    async.warn = _console_fn('warn');                                                             // 1082
    async.error = _console_fn('error');*/                                                         // 1083
                                                                                                  // 1084
    async.memoize = function (fn, hasher) {                                                       // 1085
        var memo = {};                                                                            // 1086
        var queues = {};                                                                          // 1087
        var has = Object.prototype.hasOwnProperty;                                                // 1088
        hasher = hasher || identity;                                                              // 1089
        var memoized = _restParam(function memoized(args) {                                       // 1090
            var callback = args.pop();                                                            // 1091
            var key = hasher.apply(null, args);                                                   // 1092
            if (has.call(memo, key)) {                                                            // 1093
                async.setImmediate(function () {                                                  // 1094
                    callback.apply(null, memo[key]);                                              // 1095
                });                                                                               // 1096
            }                                                                                     // 1097
            else if (has.call(queues, key)) {                                                     // 1098
                queues[key].push(callback);                                                       // 1099
            }                                                                                     // 1100
            else {                                                                                // 1101
                queues[key] = [callback];                                                         // 1102
                fn.apply(null, args.concat([_restParam(function (args) {                          // 1103
                    memo[key] = args;                                                             // 1104
                    var q = queues[key];                                                          // 1105
                    delete queues[key];                                                           // 1106
                    for (var i = 0, l = q.length; i < l; i++) {                                   // 1107
                        q[i].apply(null, args);                                                   // 1108
                    }                                                                             // 1109
                })]));                                                                            // 1110
            }                                                                                     // 1111
        });                                                                                       // 1112
        memoized.memo = memo;                                                                     // 1113
        memoized.unmemoized = fn;                                                                 // 1114
        return memoized;                                                                          // 1115
    };                                                                                            // 1116
                                                                                                  // 1117
    async.unmemoize = function (fn) {                                                             // 1118
        return function () {                                                                      // 1119
            return (fn.unmemoized || fn).apply(null, arguments);                                  // 1120
        };                                                                                        // 1121
    };                                                                                            // 1122
                                                                                                  // 1123
    function _times(mapper) {                                                                     // 1124
        return function (count, iterator, callback) {                                             // 1125
            mapper(_range(count), iterator, callback);                                            // 1126
        };                                                                                        // 1127
    }                                                                                             // 1128
                                                                                                  // 1129
    async.times = _times(async.map);                                                              // 1130
    async.timesSeries = _times(async.mapSeries);                                                  // 1131
    async.timesLimit = function (count, limit, iterator, callback) {                              // 1132
        return async.mapLimit(_range(count), limit, iterator, callback);                          // 1133
    };                                                                                            // 1134
                                                                                                  // 1135
    async.seq = function (/* functions... */) {                                                   // 1136
        var fns = arguments;                                                                      // 1137
        return _restParam(function (args) {                                                       // 1138
            var that = this;                                                                      // 1139
                                                                                                  // 1140
            var callback = args[args.length - 1];                                                 // 1141
            if (typeof callback == 'function') {                                                  // 1142
                args.pop();                                                                       // 1143
            } else {                                                                              // 1144
                callback = noop;                                                                  // 1145
            }                                                                                     // 1146
                                                                                                  // 1147
            async.reduce(fns, args, function (newargs, fn, cb) {                                  // 1148
                fn.apply(that, newargs.concat([_restParam(function (err, nextargs) {              // 1149
                    cb(err, nextargs);                                                            // 1150
                })]));                                                                            // 1151
            },                                                                                    // 1152
            function (err, results) {                                                             // 1153
                callback.apply(that, [err].concat(results));                                      // 1154
            });                                                                                   // 1155
        });                                                                                       // 1156
    };                                                                                            // 1157
                                                                                                  // 1158
    async.compose = function (/* functions... */) {                                               // 1159
        return async.seq.apply(null, Array.prototype.reverse.call(arguments));                    // 1160
    };                                                                                            // 1161
                                                                                                  // 1162
                                                                                                  // 1163
    function _applyEach(eachfn) {                                                                 // 1164
        return _restParam(function(fns, args) {                                                   // 1165
            var go = _restParam(function(args) {                                                  // 1166
                var that = this;                                                                  // 1167
                var callback = args.pop();                                                        // 1168
                return eachfn(fns, function (fn, _, cb) {                                         // 1169
                    fn.apply(that, args.concat([cb]));                                            // 1170
                },                                                                                // 1171
                callback);                                                                        // 1172
            });                                                                                   // 1173
            if (args.length) {                                                                    // 1174
                return go.apply(this, args);                                                      // 1175
            }                                                                                     // 1176
            else {                                                                                // 1177
                return go;                                                                        // 1178
            }                                                                                     // 1179
        });                                                                                       // 1180
    }                                                                                             // 1181
                                                                                                  // 1182
    async.applyEach = _applyEach(async.eachOf);                                                   // 1183
    async.applyEachSeries = _applyEach(async.eachOfSeries);                                       // 1184
                                                                                                  // 1185
                                                                                                  // 1186
    async.forever = function (fn, callback) {                                                     // 1187
        var done = only_once(callback || noop);                                                   // 1188
        var task = ensureAsync(fn);                                                               // 1189
        function next(err) {                                                                      // 1190
            if (err) {                                                                            // 1191
                return done(err);                                                                 // 1192
            }                                                                                     // 1193
            task(next);                                                                           // 1194
        }                                                                                         // 1195
        next();                                                                                   // 1196
    };                                                                                            // 1197
                                                                                                  // 1198
    function ensureAsync(fn) {                                                                    // 1199
        return _restParam(function (args) {                                                       // 1200
            var callback = args.pop();                                                            // 1201
            args.push(function () {                                                               // 1202
                var innerArgs = arguments;                                                        // 1203
                if (sync) {                                                                       // 1204
                    async.setImmediate(function () {                                              // 1205
                        callback.apply(null, innerArgs);                                          // 1206
                    });                                                                           // 1207
                } else {                                                                          // 1208
                    callback.apply(null, innerArgs);                                              // 1209
                }                                                                                 // 1210
            });                                                                                   // 1211
            var sync = true;                                                                      // 1212
            fn.apply(this, args);                                                                 // 1213
            sync = false;                                                                         // 1214
        });                                                                                       // 1215
    }                                                                                             // 1216
                                                                                                  // 1217
    async.ensureAsync = ensureAsync;                                                              // 1218
                                                                                                  // 1219
    async.constant = _restParam(function(values) {                                                // 1220
        var args = [null].concat(values);                                                         // 1221
        return function (callback) {                                                              // 1222
            return callback.apply(this, args);                                                    // 1223
        };                                                                                        // 1224
    });                                                                                           // 1225
                                                                                                  // 1226
    async.wrapSync =                                                                              // 1227
    async.asyncify = function asyncify(func) {                                                    // 1228
        return _restParam(function (args) {                                                       // 1229
            var callback = args.pop();                                                            // 1230
            var result;                                                                           // 1231
            try {                                                                                 // 1232
                result = func.apply(this, args);                                                  // 1233
            } catch (e) {                                                                         // 1234
                return callback(e);                                                               // 1235
            }                                                                                     // 1236
            // if result is Promise object                                                        // 1237
            if (_isObject(result) && typeof result.then === "function") {                         // 1238
                result.then(function(value) {                                                     // 1239
                    callback(null, value);                                                        // 1240
                })["catch"](function(err) {                                                       // 1241
                    callback(err.message ? err : new Error(err));                                 // 1242
                });                                                                               // 1243
            } else {                                                                              // 1244
                callback(null, result);                                                           // 1245
            }                                                                                     // 1246
        });                                                                                       // 1247
    };                                                                                            // 1248
                                                                                                  // 1249
    // Node.js                                                                                    // 1250
    if (typeof module === 'object' && module.exports) {                                           // 1251
        module.exports = async;                                                                   // 1252
    }                                                                                             // 1253
    // AMD / RequireJS                                                                            // 1254
    else if (typeof define === 'function' && define.amd) {                                        // 1255
        define([], function () {                                                                  // 1256
            return async;                                                                         // 1257
        });                                                                                       // 1258
    }                                                                                             // 1259
    // included directly via <script> tag                                                         // 1260
    else {                                                                                        // 1261
        root.async = async;                                                                       // 1262
    }                                                                                             // 1263
                                                                                                  // 1264
}());                                                                                             // 1265
                                                                                                  // 1266
////////////////////////////////////////////////////////////////////////////////////////////////////

}).call(this);






(function () {

////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                //
// packages/peerlibrary:async/after.js                                                            //
//                                                                                                //
////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                  //
async = module.exports;                                                                           // 1
                                                                                                  // 2
////////////////////////////////////////////////////////////////////////////////////////////////////

}).call(this);

////////////////////////////////////////////////////////////////////////////////////////////////////////////

}).call(this);


/* Exports */
Package._define("peerlibrary:async", {
  async: async
});

})();
