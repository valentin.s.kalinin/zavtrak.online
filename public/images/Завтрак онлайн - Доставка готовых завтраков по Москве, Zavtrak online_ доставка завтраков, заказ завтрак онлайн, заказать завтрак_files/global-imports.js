/* Imports for global scope */

Mongo = Package.mongo.Mongo;
ReactiveVar = Package['reactive-var'].ReactiveVar;
Tracker = Package.tracker.Tracker;
Deps = Package.tracker.Deps;
check = Package.check.check;
Match = Package.check.Match;
HTTP = Package.http.HTTP;
ServiceConfiguration = Package['service-configuration'].ServiceConfiguration;
async = Package['peerlibrary:async'].async;
Meteor = Package.meteor.Meteor;
global = Package.meteor.global;
meteorEnv = Package.meteor.meteorEnv;
WebApp = Package.webapp.WebApp;
_ = Package.underscore._;
DDP = Package['ddp-client'].DDP;
Blaze = Package.ui.Blaze;
UI = Package.ui.UI;
Handlebars = Package.ui.Handlebars;
Spacebars = Package.spacebars.Spacebars;
Template = Package['templating-runtime'].Template;
meteorInstall = Package.modules.meteorInstall;
meteorBabelHelpers = Package['babel-runtime'].meteorBabelHelpers;
Promise = Package.promise.Promise;
Accounts = Package['accounts-base'].Accounts;
Autoupdate = Package.autoupdate.Autoupdate;
Reload = Package.reload.Reload;
HTML = Package.htmljs.HTML;
Symbol = Package['ecmascript-runtime-client'].Symbol;
Map = Package['ecmascript-runtime-client'].Map;
Set = Package['ecmascript-runtime-client'].Set;

