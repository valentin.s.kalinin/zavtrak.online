import { Mongo } from 'meteor/mongo';

Products = new Mongo.Collection('products');
Categories = new Mongo.Collection('categories');
Additions = new Mongo.Collection('additions');

Orders = new Mongo.Collection('orders');
Delivery = new Mongo.Collection('delivery');
Staff = new Mongo.Collection('staff');

Favorites = new Meteor.Collection('favorites');
QrTokens = new Meteor.Collection('qrtokens');

Promo = new Meteor.Collection('promo');

DeliveryGeo = new Mongo.Collection('deliverygeo');

UserDelivery = new Mongo.Collection('userdelivery');

Messages = new Mongo.Collection('messages');