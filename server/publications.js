
import '../imports/collections.js';

Meteor.publish('staff', function() {
    
   
      this.unblock();
      let userId = this.userId;
      // console.log('#staff', this.userId)
      return this.autorun(()=>{
        
        let user = Staff.findOne({userId: userId})        
        if( user ) 
        {
            // let ids = _.pluck(Staff.find().fetch(),"userId");                        
            const fields = { profile: 1, status: 1, staff: 1, emails: 1};
            // return [Meteor.users.find({_id: {$in:ids}},fields,fields), Staff.find(),Promo.find()]
            return [Meteor.users.find({},fields,fields), Staff.find(),Promo.find()]
        }
 
      })
      
    });
  
    

  
  Meteor.publish('categories', function() {
    
    this.unblock();
    let userId = this.userId;
    // console.log('#categories published', userId)
    return this.autorun(()=>{
      
      let user = Staff.findOne({userId: userId})        
      // console.log('#categories staff:', user? 'yes': 'no')
      if( user ) 
      {       
        return Categories.find();
      }
     
        let queryCollections = [{enabled:true}]

        if( !Favorites.find({userId: userId}).count() )
        {
          queryCollections.push({ name:{$ne: 'favorites'}})
        }
        // console.log('#categories', queryCollections, );
        if( Categories.find({$and:queryCollections}).count() )
        {
           return Categories.find({$and:queryCollections});
        }
      
        return Categories.find({});   
    })
      
});
  
Meteor.publish('messages', function() {
  
    // console.log('#messages', this.userId)
    
    if( Staff.findOne({userId: this.userId})) 
    {
      return Messages.find();
    }

    return Messages.find({userId: this.userId});
    
  });
  
  Meteor.publish('qrtokens', function() {
  
    // console.log('#qrtokens', this.userId)
    
    if( Staff.findOne({userId: this.userId})) 
    {
      return QrTokens.find();
    }

  });


  Meteor.publish('products', function() {
  
    // console.log('#products', this.userId)
    
    if( Staff.findOne({userId: this.userId})) 
    {
      return [Products.find(), Additions.find()];
    }

    return [Products.find(),Favorites.find(),Additions.find()];
    
  });
  
  Meteor.publish('kitchen-orders', function() {
    
    this.unblock();
    let userId = this.userId;
    // console.log('#orders', this.userId)
    return this.autorun(()=>{
        
        let user = Staff.findOne({userId: userId})        
        if( user ) 
        {       
            var today = new Date();
            today.setHours(0,0,0,0);
        
            var tomorrow = new Date();
            tomorrow.setDate(today.getDate()+1);
            tomorrow.setHours(23,59,59,999);
            
            return Orders.find({ $or: [ { timestamp: {$gte: today, $lt: tomorrow}} , { delivery: {$gte: today, $lt: tomorrow}}]});
        }
        return Orders.find({userId: userId, hidden: { $ne: true } });
      })

  });

  Meteor.publish('orders', function(skip, limit) {

    this.unblock();
    let userId = this.userId;
    // console.log('#orders', this.userId, skip, limit)
    return this.autorun(()=>{
        
        let user = Staff.findOne({userId: userId})        
        if( user ) 
        { 
          let cursor = Orders.find({hidden: { $ne: true } },{skip: skip, limit: limit, sort: { delivery: -1 }});
          // console.log("#orders count:",cursor.fetch().length, cursor.count());
          return cursor;
        }
        return Orders.find({userId: userId, hidden: { $ne: true } });
      })
  });

  Meteor.publish('analytics', function(fromDate, toDate) {

    this.unblock();
    let userId = this.userId;
    // console.log('#orders', this.userId, skip, limit)
    return this.autorun(()=>{
        
        let user = Staff.findOne({userId: userId})        
        if( user ) 
        { 
          
          const startDate = new Date(today.getFullYear(), today.getMonth(), today.getDate());
          const endDate = new Date(today.getFullYear(), today.getMonth(), today.getDate() + 1);

          let cursor = Orders.find({hidden: { $ne: true }, delivery: {$gte: startDate, $lt: endDate} },{sort: { delivery: -1 }});
          console.log('#analytics orders count:',cursor.fetch().length, cursor.count());
          return cursor;
        }
      })
  });

  Meteor.publish('order', function(id) {

    this.unblock();
    let userId = this.userId;
    // console.log('#order', this.userId, id)
    return this.autorun(()=>{
        
        let user = Staff.findOne({userId: userId})        
        if( user ) 
        {       
          return Orders.find({_id: id});
        }
      })
  });

  Meteor.publish('deliverygeo', function() {
  
    console.log('#deliverygeo', this.userId)
    
    if( Staff.findOne({userId: this.userId})) 
    {
      return DeliveryGeo.find();
    }
    
  });
  