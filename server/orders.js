
import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check'
import { Match } from 'meteor/check'
import { HTTP } from 'meteor/http'
import { Random } from 'meteor/random'
import { Email } from 'meteor/email'

import fecha from 'fecha';

import { byteLength, str2rstr_utf8, hex_hmac_sha1, hex2bin } from '../imports/mac.js';
import '../imports/collections.js';

var OrderNumber = new Meteor.Collection('order_number');

// Meteor.publish('categories', function (roomId) {
//   // Make sure `roomId` is a string, not an arbitrary Mongo selector object.
//   check(roomId, String);
//   return Chats.find({ room: roomId });
// });

// Meteor.publish('products', function listsPublic() {
//   return Lists.find({
//     userId: { $exists: false },
//   }, {
//     fields: Lists.publicFields,
//   });
// });

// Meteor.publish('lists.private', function listsPrivate() {
//   if (!this.userId) {
//     return this.ready();
//   }

//   return Lists.find({
//     userId: this.userId,
//   }, {
//     fields: Lists.publicFields,
//   });
// });


Meteor.methods({

  // pay: online, cash, card
  applyPromo(id,promo)
  {
    let order = Orders.findOne(id);
    console.log("#applyPromo",id,promo);
    
    var total = '0';
    _.each(order.cart,(entry)=> total = parseFloat(parseFloat(total) + parseFloat(entry.quantity) * parseFloat(entry.item.price)))
    
    let discount = Meteor.call('getPromoDiscount',promo);
    let totalWithDiscount = total;

    if( discount )
    {
      totalWithDiscount = (total - ((total / 100) * discount)).toFixed(2);
    }
    const entity = Object.assign({}, order, 
    { promo: promo} ,
    { discount: discount },
    { discountTimestamp: new Date().getTime()}, 
    { total: total},
    { totalWithDiscount: totalWithDiscount})

    let result = Orders.update({_id:id},entity)
    console.log("#applyPromo",result,entity);
  },

  generateReport(fromDate,toDate)
  {
    console.log("generateReport",fromDate,toDate); 

    // var cursor  = Orders.find();
    
    let staff = Staff.findOne({userId: this.userId})        
    if( !staff ) 
    { 
      return;
    }
    let user = Meteor.users.findOne({_id: this.userId});

    let cursor = Orders.find({'status':{$ne: 'cancelled'}, hidden: { $ne: true }, delivery: {$gte: fromDate, $lt: toDate} },{sort: { delivery: -1 }});
    console.log("#generateReport orders count:",cursor.count());
  
    var data = cursor.fetch();

    _.each(data,(el)=>{
       _.map(el.cart,(item)=> {
         return item.delivery = el.delivery;
       });
    })

    var array = _.flatten(_.pluck(data,'cart'));

    var map = _.map(array,function(el){ 
        el.item.count = el.quantity ; 
        return _.pick(el.item,'title','count','price');
    });
    
    var group = _.groupBy(map,(el)=>el.title);
    
    var map = _.map(group,function(el,key){
      var count = _.reduce(el, function(memo, el){ return memo + el.count; }, 0); 
      return{ 'item':key,'count':count};       
    });
    
    var report = _.sortBy(map,(el)=>_.keys(el))
    console.log('report', report)

    var fromStr = fecha.format(fromDate,"DD:MM:YYYY");
    var toStr = fecha.format(toDate,"DD:MM:YYYY");
   
    var title = "Отчет с " + fromStr + ' по '  + toStr + '\n'; 
    var text = title; 
    _.each(report,(row)=> text += 'Товар: ' + row.item + ' Всего: ' + row.count + '\n');
    
    //console.log(text);
    console.log(user.emails[0].address);
    // return text;    
    Meteor.call(
      'sendEmail',
      user.emails[0].address,
      'notification@zavtrak.online',
      title,
      text    
    );
  },

  sendEmail(to, from, subject, text) {
    // Make sure that all arguments are strings.
    check([to, from, subject, text], [String]);

    // Let other method calls from the same client start running, without
    // waiting for the email sending to complete.
    this.unblock();

    Email.send({ to, from, subject, text });
  },

  newOrder(order) {
    // console.log("#newOrder",order)
    var start = Date.now();
    // check(order, {
    //   address: String,
    //   promo: Match.Maybe(String),
    //   userId: Match.Maybe(String),
    //   name: Match.Maybe(String),
    //   comment: Match.Maybe(String),
    //   phone: String,
    //   delivery: Date,
    //   pay: String,    
    //   cart: Array        
    // });

    var number = 999999 + OrderNumber.find().count();
    var total = 0;
    _.each(order.cart,(entry)=> total = parseFloat(parseFloat(total) + parseFloat(entry.quantity) * parseFloat(entry.item.price)))
    
    let discount = order.promo ? Meteor.call('getPromoDiscount',order.promo): undefined;
    // console.log("#discount",order.promo, discount);

    let totalWithDiscount = total;
    
    if( discount )
    {
      totalWithDiscount = (total - ((total / 100) * discount)).toFixed(2);
    }

    let sumTime = (time)=>time.getFullYear()+time.getMonth()+time.getDate()
    let timestamp = new Date(); 
    let zone = DeliveryGeo.findOne({index: order.deliveryZoneIndex});

    const entity = Object.assign({}, order, 
      { status: "new" },
      { payment: '' },
      { discount: discount },
      { deliveryZoneName: zone ? zone.name : 'Вне зоны доставки'},
      { timestamp: timestamp.getTime()}, 
      { total: total},
      { totalWithDiscount: totalWithDiscount},
      { today : sumTime(order.delivery) == sumTime(timestamp) },
      { number : number}
    );
    
    Meteor.users.update({_id: order.userId }, 
      {$set: 
        { 'profile.delivery' : order.delivery ,
          'profile.phone': order.phone, 
          'profile.address': order.address, 
          'profile.comment': order.comment, 
          'profile.name': order.name }
        });

    var orderId = Orders.insert(entity);
   
    OrderNumber.insert({order: orderId, number: number})
    // console.log("#newOrder 1", start - Date.now());
    this.unblock(); 
    try {
      Meteor.call('notifyNewOrder', orderId)
    } catch (error) {
      console.log(error)
    }

    return Orders.findOne({_id: orderId});
  },
  notifyNewOrder:function(orderId)
  {
    var order = Orders.findOne({ _id: orderId });
    var user = Meteor.users.findOne({_id: order.userId});
    
    var notifyOnly = user.emails[0].address;

    var notifyStaff = Meteor.users.find({staff: true, 'emails.0.notification': true}).fetch();
    _.each(notifyStaff, (user)=>{

        if( user.emails[0].notify_on_email )
        {
          if (user.emails[0].notify_on_email === notifyOnly) {
            Meteor.call(
              'sendEmail',
              user.emails[0].address,
              'notification@zavtrak.online',
              'Новый заказ',
              Meteor.absoluteUrl() + 'order?_id=' + orderId
            );
          }
        }
       else
        {
          Meteor.call(
            'sendEmail',
            user.emails[0].address,
            'notification@zavtrak.online',
            'Новый заказ',
            Meteor.absoluteUrl() + 'order?_id=' + orderId
          );
        }
        
    } )
  },
  notifyOrderCancelled:function(orderId)
  {
    var notifyStaff = Meteor.users.find({staff: true, 'emails.0.notification': true}).fetch();
    _.each(notifyStaff, (user)=>{
        Meteor.call(
          'sendEmail',
          user.emails[0].address,
          'notification@zavtrak.online',
          'Заказ отменен',
          Meteor.absoluteUrl()+ 'order?_id='+ orderId
        );
    } )
  },
  _testPayment(orderId){
    var payment = {"requestId":"a952cbfc-8ea4-4eae-80e4-a3e1af7d419d","methodName":"basic-card","details":{"billingAddress":{"addressLine":["Кожуховская"],"city":"Москва","country":"RU","dependentLocality":"","languageCode":"","organization":"","phone":"+79161845136","postalCode":"123456","recipient":"Валентин Калинин","region":"город Москва","sortingCode":""},"cardNumber":"4402040053336931","cardSecurityCode":"725","cardholderName":"IVAN IVANOV","expiryMonth":"03","expiryYear":"2019"},"shippingAddress":{"country":"RU","addressLine":["Кожуховская"],"region":"город Москва","city":"Москва","dependentLocality":"","postalCode":"123456","sortingCode":"","languageCode":"","organization":"","recipient":"Валентин Калинин","phone":"+79161845136"},"shippingOption":"moscow","payerName":"Валентин Калинин","payerEmail":null,"payerPhone":"+79161845136"}
    
    console.log("#test payment", payment)
    return Meteor.call('processPayment',orderId, payment); 
    
  },  
  addPromoCode(name, value)
  {
    console.log("#addPromoCode", name,value)
    Promo.upsert({name:name},{name:name, discount: value});
  },
  removePromo(id)
  {
    console.log("#removePromoCode", id)
    Promo.remove({_id:id});
  },
  resetPromo()
  {
    console.log("#resetPromoCodes")
    Promo.remove({});
  },
  getPromoDiscount(promo)
  {
    let code = Promo.findOne({name: promo}) 
    console.log("promo",promo, code)
    if( code )
    {
      return code.discount;
    }    
  },
  paymentCompleted(payment){
    // check if we not abused P_SIGN
    // 
    var number = parseInt(payment.order);
    console.log("paymentCompleted", number);
    Orders.update({'number': number},{$set:{'payment': 'completed'}});    
  },

  processPayment(_id, payment)
  {

    // var testUrl = 'https://3dst.sdm.ru/cgi-bin/cgi_link';
    // var secret_test = '466B3FE46B9D6030B322EEFAB03BE966';


    var secret = '131EE3B9CAC25C0AF6A9222224A3D727'

    var order = Orders.findOne(_id);
    // console.log("#generatePayment", order, payment);
    // order.timestamp ||
   
    var data = {
      amount: order.total,
      currency: '643',
      order: order.number,
      desc: "Zavtrak.online purchase", //order.descr || 'Заказ завтрака онлайн',
      merch_name: 'EC ZAVTRAK.ONLINE',
      merch_url: 'https://zavtrak.online',

      merchant:'01536830',
      terminal: '01143670',
 
      merch_gmt: '+3',
      email: '',
      trtype: '1',
      timestamp: fecha.format(Date.now(),"YYYYMMDDHHmmSS"),
      
      nonce: Random.hexString(16).toUpperCase(),
      backref: 'https://zavtrak.online/checkout/success/',  
      mac_data: ''
    }

    // var dataTest = {
    //   amount: '104.21',
    //   currency: '643',
    //   order: '963344',
    //   desc: 'Valenki premium, size 48',
    //   merch_name: 'Valenki Online Inc.',
    //   merch_url: 'https://3dsdemo.sdm.ru/',

    //   merchant:'123456789012345',
    //   terminal: '10008888',
 
    //   merch_gmt: '+3',
    //   email: '',
    //   trtype: '1',
    //   timestamp: '20171128201621',
      
    //   nonce: 'F2B2DD7E603A7ADA',
    //   backref: 'https://3dsdemo.sdm.ru/',  
    //   mac_data: ''
    // }
    // data = dataTest;

    _.each(_.values(_.omit(data,"merch_gmt","mac_data")),(value)=>{
     
      if(value === "")
      {
        data.mac_data+="-";
      }else
      {
        data.mac_data+=byteLength(value.toString())+str2rstr_utf8(value.toString())        
      }     
    });
    
    data.mac = hex_hmac_sha1(hex2bin(secret),data.mac_data);
    data.p_sign = data.mac;

    return data;

  },
  resetOrder(_id){
    Meteor.call('addOrderState',_id,"new");     
  },
  cancelOrder(_id) {
    Meteor.call('addOrderState',_id,"cancelled"); 
    this.unblock(); 
    try {
      Meteor.call('notifyOrderCancelled', orderId)
    } catch (error) {
      console.log(error)
    }    
  },
  closeOrder(_id) {
    Meteor.call('addOrderState',_id,"closed");
    Orders.update({'_id': _id},{$set:{'payment': 'completed'}});  
  },

  openOrder(_id) {    
    Meteor.call('addOrderState',_id,"opened");    
    Meteor.call("cookingOrder",_id);
},

hideOrder(_id) {
  console.log("hideOrder", _id)
  Orders.update({_id: _id},{$addToSet: {'actions': { 'state': 'hide', 'by': this.userId , 'time': Date.now()}}});  
  Orders.update({_id: _id},{$set:{'hidden': true}});  
  Meteor.call('addOrderState',_id,"hidden");
},

cookedOrder(_id)
{
  Meteor.call('addOrderState',_id,"cooked");    
},

cookingOrder(_id) {    
    Meteor.call('addOrderState',_id,"cooking");    
},
deliveringOrder(_id) {    
    Meteor.call('addOrderState',_id,"delivering");
},
addOrderState(_id, state)
{
    Orders.update({_id: _id},{$addToSet: {'actions': { 'state': state, 'by': this.userId , 'time': Date.now()}}});
    Orders.update({_id: _id},{$set:{'status':state}});  
},
issueOrder(_id) {    
    Meteor.call('addOrderState',_id,"issued");    
},
resolveOrder(_id) {    
    Meteor.call('addOrderState',_id,"resolved");    
},

resetOrders() {
   
    Orders.remove({});

  }

});

// Meteor.publish('Categorys', function listsPublic() {
//   return Lists.find({
//     userId: { $exists: false },
//   }, {
//     fields: Lists.publicFields,
//   });
// });

// Meteor.publish('lists.private', function listsPrivate() {
//   if (!this.userId) {
//     return this.ready();
//   }

//   return Lists.find({
//     userId: this.userId,
//   }, {
//     fields: Lists.publicFields,
//   });
// });