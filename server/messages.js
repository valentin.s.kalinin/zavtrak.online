
import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check'

import '../imports/collections.js';

Meteor.methods({

  sendMessage(message, contact) {
    console.log("#sendMessage",message)

    const entity = Object.assign({}, 
      { contact: contact },
      { message : message }, 
      { userId: this.userId },
      { status: "new" },
      { timestamp: new Date().getTime()}, 
    );

    var result = Messages.insert(entity);
    Meteor.call('notifyNewMessage',result);
    return result;
  },

  replyMessage(id, message) {
    
    const entity = Object.assign({},
        { messageId: id},
        { message : message}, 
        { userId: this.userId },
        { status: "new" },
        { timestamp: new Date().getTime()}, 
      );
  
      return Messages.insert(entity);
  },

  notifyNewMessage:function(messageId)
  {
    var message = Messages.findOne(messageId);

    var notifyStaff = Meteor.users.find({staff: true, 'emails.0.notification': true}).fetch();
    _.each(notifyStaff, (user)=>{
        Meteor.call(
          'sendEmail',
          user.emails[0].address,
          'notification@zavtrak.online',
          'Новое сообщение',
          message.contact + " : " + message.message    
        );
    } )
  },

  removeMessage(id)
  {
    console.log("#removeMessage", id)
    Messages.remove({_id:id});
  },
  resetMessages()
  {
    console.log("#resetMessages")
    Messages.remove({});
  },
 
  resetMessage(_id){
    Meteor.call('addMessageState',_id,"new");     
  },
  cancelMessage(_id) {
    Meteor.call('addMessageState',_id,"cancelled"); 
  },
  closeMessage(_id) {
    Meteor.call('addMessageState',_id,"closed");
  },

  openMessage(_id) {    
    Meteor.call('addMessageState',_id,"opened");    
    },

    hideMessage(_id) {
        Messages.update({_id: _id},{$addToSet: {'actions': { 'state': 'hide', 'by': this.userId , 'time': Date.now()}}});
        Messages.update({_id: _id},{$set:{'hidden': true}});  
    },

    addMessageState(_id, state)
    {
        Messages.update({_id: _id},{$addToSet: {'actions': { 'state': state, 'by': this.userId , 'time': Date.now()}}});
        Messages.update({_id: _id},{$set:{'status':state}});  
    }

});
