
import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check'
import { Match } from 'meteor/check'

import '../imports/collections.js';

// Meteor.publish('categories', function (roomId) {
//   // Make sure `roomId` is a string, not an arbitrary Mongo selector object.
//   check(roomId, String);
//   return Chats.find({ room: roomId });
// });

Meteor.methods({

  initAdditions()
  {   
      Meteor.call("addAddition",{name:"курага", price: "20"});
      Meteor.call("addAddition",{name:"орехи", price: "30"});
      Meteor.call("addAddition",{name:"свежие ягоды", price: "40"});
      Meteor.call("addAddition",{name:"варенье", price: "50"});
 
  },
   
  addAddition(addition) {
    check(addition, {
      name: String,
      price: String,
    });
  
    Additions.insert(addition);

  },
  resetAdditions() {
   
    Additions.remove({});

  }

});

// Meteor.publish('Categorys', function listsPublic() {
//   return Lists.find({
//     userId: { $exists: false },
//   }, {
//     fields: Lists.publicFields,
//   });
// });

// Meteor.publish('lists.private', function listsPrivate() {
//   if (!this.userId) {
//     return this.ready();
//   }

//   return Lists.find({
//     userId: this.userId,
//   }, {
//     fields: Lists.publicFields,
//   });
// });