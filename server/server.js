import { Accounts } from 'meteor/accounts-base'

Meteor.startup(function () {
    // process.env['NODE_TLS_REJECT_UNAUTHORIZED'] = 0;
    process.env.MAIL_URL = "smtp://postmaster@zavtrak.online:f0bf7b236d7822e8e4902df8e7b143f6@smtp.mailgun.org:587";
    // process.env.MAIL_URL = "smtp://postmaster@sandbox77517.mailgun.org:7r3l2idm3k64@smtp.mailgun.org:587";

    Accounts.config({sendVerificationEmail: true, forbidClientAccountCreation: false});
  ''
    Accounts.urls.resetPassword = function (token) {
        return Meteor.absoluteUrl('reset/' + token);
    };

    WebApp.connectHandlers.use('/payment',(req, res, next) => {
        
        console.log("#payment callback received");
        var body = "";
        req.on('data', Meteor.bindEnvironment(function (data) {
            body += data;
        }));

        req.on('end', Meteor.bindEnvironment(function () {
        
            var keys = []
            var values = [];
            
            _.each(body.split('&'),(item)=>{
              var pair = item.split('=')
              keys.push(pair[0])
              values.push(pair[1])
            })
            var responce = _.object(keys,values);
            
            if( responce['Result'] === '0' && responce['RC'] === '00' )
            {
                Meteor.call('paymentCompleted', {order: responce['Order']});
            }
            
            // console.log( )
            
            next();

        }));

      });
});