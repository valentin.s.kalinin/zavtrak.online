import { HTTP } from 'meteor/http'
// import * as admin from "firebase-admin";

Meteor.methods({
    'order.toggleNotification': function(userId, value)
    {        
        var result = Meteor.users.update({_id: userId}, {$set:{'emails.0.notification': value}});
        console.log("order.toggleNotification",userId, result, value)
    },
    'account.toggleNotification': function (userId, value) {
        var result = Meteor.users.update({ _id: userId }, { $set: { 'emails.0.notify_on_email': value } });
        console.log("account.toggleNotification", userId, result, value)
    },
    'user.subscription': function(userId, string)
    {
        check(userId,String)
        check(string,String)
        
        Meteor.users.update({_id: userId},{$set:{ 'subscription': string}});
        // console.log("user.subscription", userId, string );
    },
    'push.me':function (userId)
    {
        check(userId,String)
        console.log("we here?")
        // var user = Meteor.users.findOne(userId);
        // if(!user)
        //     throw new Meteor.Error(404, "User not found");
        
        this.unblock();
        // var subscription = user.subscription;        
        console.log("push.me", userId, subscription)
        
        var headers = {
            'Content-Type': 'application/json',
            'Authorization': 'key=AAAAlGfQb8g:APA91bGTn2oeqHhFz5DxJVDzW57DYGx-ih87lD-6ZY3t7E0H0gUkxWqI_Nz7aDAl0pBuZ-FEofF39l32ETlGKubl3kOzZ_yambyK33m4IXblVRn8wspX53f79V_jeiQqzthMTecevf51' 
        };
     
        // var subscription = 'dewW2lqa4x0:APA91bEgVHuXQ3cjD6vl70KHV-9KoZzuz4u1C3obP3W-fA_gJ3zg5J7mY6aKxwaYKpkTJdLuRpToWlJ38ENoOkFvr9lewYWGb23qrlGAQBxa2IZ5YJTTbVQq2yBWkBnLOZkvWrG4RIjD'
        var subscription = 'fupaDZr43A8:APA91bHa5fH7H4YUZoM3QIOp6yJaqcAziUBMHZnKpZb--a27XPsAX1-KPZGsGL64v1KSLOY1c44mk6B51lnPygY94mluev8XFJXyWweh-SOnD-uqc0RFO2xQZ2dTalyHYYkLwvZRODoS'
        var data = {
            'to': subscription,
            'notification': {
                'body': "Hello world body!",
                'title': "Hello world title!",
                'sound': 'default'
            },
            "content_available": true,
            "priority": "high",
        };
       
        HTTP.call('POST', 'https://fcm.googleapis.com/fcm/send', {headers: headers, data: data}, function (err, res) {
            if (err)
            {
                console.log(err);
            }else{
                console.log("Send push notification",res);   
            }                      
        });

        // const payload = JSON.stringify({
        //     title: 'Welcome',
        //     body: 'Thank you for enabling push notifications',
        //     icon: '/android-chrome-192x192.png'
        //   });
      
        //   const options = {
        //     TTL: 86400
        //   };

        //   webpush.sendNotification(
        //     subscription, 
        //     payload,
        //     options
        //     ).then(function() {
        //       console.log("Send welcome push notification");
        //     }).catch(err => {
        //       console.error("Unable to send welcome push notification", err );
        //   });


    },
   
    
});


