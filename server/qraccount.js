import { Accounts } from 'meteor/accounts-base'
import { Random } from 'meteor/random'

Meteor.methods({
    
      // pay: online, cash, card
      addQRTokenAccount(name, address, phone) {
        // console.log("enrollStaff", email);
        let staff = Staff.findOne({userId: this.userId});

        if( staff )
        {
          var userId = Accounts.createUser({username: name, profile: { name: name, address: address, phone: phone } });
          QrTokens.insert({ userId: userId, name: name, address: address, phone: phone, token: Random.secret()});
        }
       
      },
        // pay: online, cash, card
        updateQRTokenAccount(id, userId, name, address, phone) {
          // console.log("enrollStaff", email);
          let staff = Staff.findOne({userId: this.userId});
  
          if( staff )
          {
            QrTokens.update({_id: id},
              { $set: {
                name: name, 
                address: address, 
                phone: phone
              }});

            Meteor.users.update({_id: userId }, 
              {
                  $set: { 'profile.address' : address, 'profile.phone': phone, 'profile.name': name }
              });
          }
         
        },
      removeQRTokenAccount(id, userId) {
        
        let staff = Staff.findOne({userId: this.userId});

        if( staff )
        { 
          QrTokens.remove({_id: id});
          Meteor.users.remove({_id: userId});  
        } 
       
      }
    }
);
    
Meteor.startup(function () {
  QrTokens._ensureIndex({
    token: 1
  });
});


// Login with just a token
Accounts.registerLoginHandler(function (loginRequest) {
  // Is there an auth token? If not, just let Meteor handle it. Call it dispatch_authToken in case there's another
  // library that uses authToken
  if (!loginRequest || !loginRequest.dispatch_authToken) {
    return undefined;
  }
  console.log("loginHandler", loginRequest.dispatch_authToken, QrTokens.find().fetch());
  // Find the matching user from the code
  const doc = QrTokens.findOne({
    token: loginRequest.dispatch_authToken,
  });
  


  if (!doc) {
    throw new Meteor.Error('Invalid token');
  }

  console.log(doc);

  return {
    userId: doc.userId
  };
});


