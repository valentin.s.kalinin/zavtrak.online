import { Accounts } from 'meteor/accounts-base'

Meteor.methods({
    
      // pay: online, cash, card
      enrollStaff(email) {
        // console.log("enrollStaff", email);
       
        var user = Accounts.findUserByEmail(email)
        if( user )
        {
          Meteor.call("makeStaff", user._id);
        }
       
      },

      makeStaff(userId) {
        // console.log("#makeStaff",userId)
        
        Staff.insert({userId: userId, permissions:['admin','kitchen','delivery']})
        Meteor.users.update({_id: userId},{$set:{'staff':true}});  
      },

      removeStaff(userId)
      {
        console.log("#removeStaff",userId)
        Meteor.users.update({_id: userId},{$set:{'staff':false}});  
        Staff.remove({userId: userId});
      }

    }
);
    
