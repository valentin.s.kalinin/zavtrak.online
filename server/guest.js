import {Meteor} from 'meteor/meteor'
import { Accounts } from 'meteor/accounts-base'
import { Random } from 'meteor/random'

Accounts.onCreateUser(function (options, user) {

//    if( user.services )
//        console.log('onCreateUser #Service: ', user.services);

    user.profile = options.profile || {};
    user.profile.agreement = false;

    user.createdAt = new Date().getTime();
    user.status = { lastLogin: 0, online: false }

    let guest = user.services && user.services.guest ? user.services.guest: false;
    if( guest )
    {        
        user.type = 'guest'  
        user.profile.name = guest.name
        if( guest.email )
            user.emails =[{address: guest.email, verified: false}];                                
    }
    // console.log('onCreateUser #user: ', user);  
    return user;
});

function getServiceDataFromTokens(tokens) {

    var accessToken = tokens.accessToken;
    var idToken = tokens.idToken;
    // var scopes = getScopes(accessToken);
    // var identity = getIdentity(accessToken);
    var serviceData = {
    accessToken: accessToken,
        idToken: idToken,
        };
    
    var hasOwn = Object.prototype.hasOwnProperty;
        
    if (hasOwn.call(tokens, "expiresAt")) {
        serviceData.expiresAt =
        Date.now() + 1000 * parseInt(tokens.expiresIn, 10);
    }

    // only set the token in serviceData if it's there. this ensures
    // that we don't lose old ones (since we only get this on the first
    // log in attempt)
    if (tokens.refreshToken) {
        serviceData.refreshToken = tokens.refreshToken;
    }
    // console.log("getServiceDataFromTokens", serviceData);
    return {
        serviceData: serviceData,
            options: {
                profile: {
                }
        }
    };
}

Accounts.registerLoginHandler(function (request) {
    var serviceName = '';
   
    // console.log("#login", request)
    if( request.guest ) {
        serviceName = 'guest'

        const token = Random.secret();    
        request.userId = token;
        const name = request.name;
        request.name = name;
        
        const tokenRecord = {
            token,
            name,
            when: new Date()
            };
        request.accessToken = tokenRecord;
        request.idToken = tokenRecord;
        // console.log("#request.accessToken",,token);
    }else
    {
        return;  
    }
 
    const tokens = {
      accessToken: request.accessToken,
      refreshToken: request.refreshToken,
      idToken: request.idToken,
    };
  
    const result = getServiceDataFromTokens(tokens);
    // console.log("#updateOrCreateUserFromExternalService", serviceName, request.userId, request, result.serviceData);

    var accountRes =  Accounts.updateOrCreateUserFromExternalService(serviceName, {
      id: request.userId,
      idToken: request.idToken,
      accessToken: request.accessToken,
      name: request.name || "",      
      email: request.phone || request.email || "",
      picture: request.picture || "",
      ...result.serviceData,
    }, result.options);
    
    // console.log("#accountRes",accountRes)
    return accountRes;
  });
