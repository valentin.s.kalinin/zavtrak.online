
import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check'
import { Match } from 'meteor/check'

import '../imports/collections.js';
import products from './products.json';

// Meteor.publish('products', function (roomId) {
//   // Make sure `roomId` is a string, not an arbitrary Mongo selector object.
//   check(roomId, String);
//   return Chats.find({ room: roomId });
// });


Meteor.methods({ 
  addProduct(product) {
    
    product.order  = product.order || 100;

    // check(product, {
    //   name: String,
    //   title: String,
    //   netto: String,
    //   order:  Match.Maybe(Number),
    //   timestamp: Match.Maybe(Number) ,
    //   description: String,
    //   price:  Match.Maybe(Number,String),
    //   category: String,
    //   options: Match.Maybe([String]),
    //   image: String,
    // });
  
    const entity = Object.assign({}, product, 

      { timestamp: Date.now()}

    );

    var res =  Products.insert(entity);
    console.log("#addProduct",res)
    return res;
  },
  
  updateProduct(product) {
  
    product.order  = product.order || 100;
    product.timestamp  = product.timestamp || Date.now();

    Products.update(product._id, {
      $set: product},);
  },
  removeProduct(id) {
    Products.remove(id);
  },

  addToFavorites(id)
  {
    console.log("#addToFavorites",id)
    Favorites.insert({ productId: id, userId: this.userId });
  },

  removeFromFavorites(id)
  {
    console.log("#removeFromFavorites",id, this.userId)
    Favorites.remove({ productId: id, userId: this.userId });
  },

  resetProducts() {
   
    Products.remove({});

  },
  
  initProducts(){
  
    _.each(products,(product)=>{
      Meteor.call("addProduct",_.omit(product,"_id"));
    })
    return 
    // drinks
    Meteor.call("addProduct",{ name: "Эспрессо",
      title: "Эспрессо",
      netto: "100мл",
      description: "Крепкий, как алмаз",
      price: "100",
      order: 10,
      category: "drinks",
      image: "products/эспрессо",
      largeImage: "products/эспрессо"
     });

    Meteor.call("addProduct",{ name: "Капучино",
      title: "Капучино",
      netto: "150мл",
      description: "Замечательный капучино",
      price: "150",
      order: 20,
      category: "drinks",
      image: "products/капучино",
      largeImage: "products/капучино"
     });

    Meteor.call("addProduct",{ name: "Латте",
      title: "Латте",
      netto: "150мл",
      description: "Чудо молочный кофе",
      price: "150",
      order: 30,
      category: "drinks",
      image: "products/латте",
      largeImage: "products/латте"
     });

    Meteor.call("addProduct",{ name: "Улун",
      title: "Улун",
      netto: "150мл",
      description: "Чудо молочный улун",
      price: "150",
      order: 40,
      category: "drinks",
      image: "products/чай",
      largeImage: "products/чай"
     });

    Meteor.call("addProduct",{ name: "Сенча",
      title: "Сенча",
      netto: "150мл",
      description: "Чудо сенча",
      price: "150",
      order: 50,
      category: "drinks",
      image: "products/чай",
      largeImage: "products/чай"
     });

     Meteor.call("addProduct",{ 
      name: "Ассам",
      title: "Ассам",
      netto: "150мл",
      description: "Чудо ассам",
      price: "150",
      order: 60,
      category: "drinks",
      image: "products/чай",
      largeImage: "products/чай"
     });

    Meteor.call("addProduct",{ name: "Яблоко",
      title: "Яблочный фреш",
      netto: "350мл",
      description: "Освежает сознание",
      price: "250",
      order: 70,
      category: "drinks",
      image: "products/яблоко",
      largeImage: "products/яблоко"
     });

    Meteor.call("addProduct",{ name: "Апельсин",
      title: "Апельсиновый фреш",
      netto: "350мл",
      description: "Освежает сознание",
      price: "250",
      order: 80,
      category: "drinks",
      image: "products/апельсин",
      largeImage: "products/апельсин"
     });
     
    Meteor.call("addProduct",{ name: "Грейпфрут",
      title: "Грейпфрутовый фреш",
      netto: "350мл",
      description: "Освежает сознание",
      price: "250",
      order: 90,
      category: "drinks",
      image: "products/апельсин",
      largeImage: "products/апельсин"
     });

    Meteor.call("addProduct",{ name: "Морковь",
      title: "Морковный фреш",
      netto: "350мл",
      description: "Освежает сознание",
      price: "250",
      order: 100,
      category: "drinks",
      image: "products/апельсин",
      largeImage: "products/апельсин"
     });

    Meteor.call("addProduct",{ name: "Овощной",
      title: "Овощной смузи",
      netto: "350мл",
      description: "Смузи овощной",
      price: "250",
      order: 110,
      category: "drinks",
      image: "products/апельсин",
      largeImage: "products/апельсин"
     });
    
    Meteor.call("addProduct",{ name: "Клубничный",
      title: "Клубничный смузи",
      netto: "350мл",
      description: "Смузи клубничный",
      price: "250",
      order: 120,
      category: "drinks",
      image: "products/апельсин",
      largeImage: "products/апельсин"
     });
    
    //kasha  
    Meteor.call("addProduct",{ name: "Овсянка",
      title: "Овсяная каша",
      netto: "250гр",
      description: "Овсяная каша томленая с изюмом",
      options: ['курага','орехи','свежие ягоды','варенье'],
      price: "150",
      order: 10,
      category: "kasha",
      image: "products/овсянка",
      largeImage: "products/овсянка"
     });

    Meteor.call("addProduct",{ name: "Рисовая",
      title: "Рисовая каша",
      netto: "250гр",
      description: "Рисовая каша на молоке",
      options: ['курага','орехи','свежие ягоды','варенье'],
      price: "150",
      order: 20,
      category: "kasha",
      image: "products/овсянка",
      largeImage: "products/овсянка"
     });

    Meteor.call("addProduct",{ name: "Пшенная_с_тыквой",
      title: "Пшенная каша с тыквой",
      netto: "250гр",
      description: "Пшенная каша с тыквой",
      options: ['курага','орехи','свежие ягоды','варенье'],
      price: "150",
      order: 30,
      category: "kasha",
      image: "products/пшенка",
      largeImage: "products/пшенка"
     });

    Meteor.call("addProduct",{ name: "Пшенная_с_творогом",
      title: "Пшенная каша с творогом",
      netto: "250гр",
      description: "Пшенная каша с творогом",
      options: ['курага','орехи','свежие ягоды','варенье'],
      price: "150",
      order: 40,
      category: "kasha",
      image: "products/пшенка",
      largeImage: "products/пшенка"
     });

    Meteor.call("addProduct",{ name: "Гречневая",
      title: "Гречневая с грибами и луком",
      netto: "250гр",
      description: "Гречневая с грибами и луком",
      options: ['сыр','зелень','буженина'],
      price: "150",
      order: 50,
      category: "kasha",
      image: "products/гречка",
      largeImage: "products/гречка"
     });

    //bakery
    Meteor.call("addProduct",{ name: "Французский_круассан",
      title: "Французский круассан",
      netto: "150гр",
      description: "Французский круассан",  
      price: "150",
      order: 10,
      category: "bakery",
      image: "products/круассан",
      largeImage: "products/круассан"
     });
    
    Meteor.call("addProduct",{ name: "Миндальный_круассан",
      title: "Миндальный круассан",
      netto: "150гр",
      description: "Миндальный круассан",  
      price: "150",
      order: 20,
      category: "bakery",
      image: "products/круассан_миндаль",
      largeImage: "products/круассан_миндаль"
     });

    Meteor.call("addProduct",{ name: "Кекс",
      title: "Кекс",
      netto: "250гр",
      description: "Супер кекс",  
      price: "150",
      order: 30,
      category: "bakery",
      image: "products/кекс",
      largeImage: "products/кекс"
     });

    Meteor.call("addProduct",{ name: "Венские_вафли",
      title: "Венские вафли",
      netto: "250гр",
      description: "Венские вафли",  
      price: "150",
      order: 40,
      category: "bakery",
      image: "products/вафли",
      largeImage: "products/вафли"
     });
    //pancakes
     Meteor.call("addProduct",{ name: "Блины",
      title: "Блины без начинки",
      netto: "200гр",
      description: "Блины без начинки",  
      price: "120",
      order: 10,
      category: "pancakes",
      options: ['сыр','ветчина','сметана','рыба','грибы','куриное филе','творог','яблочная начинка','варение'],   
      image: "products/блины",
      largeImage: "products/блины"
     });

    Meteor.call("addProduct",{ name: "Блины_с_семгой_и_брынзой",
      title: "Блины с семгой и брынзой",
      netto: "200гр",
      description: "Блины с семгой и брынзой",  
      price: "180",
      order: 20,
      category: "pancakes",
      options: ['сыр','ветчина','сметана','рыба','грибы','куриное филе'],   
      image: "products/блины",
      largeImage: "products/блины"
     });

    Meteor.call("addProduct",{ name: "Блины_с_сыром_и_ветчиной",
      title: "Блины с сыром и ветчиной",
      netto: "200гр",
      description: "Блины с сыром и ветчиной",  
      price: "180",
      order: 30,
      category: "pancakes",
      options: ['сыр','ветчина','сметана','рыба','грибы','куриное филе'],   
      image: "products/блины",
      largeImage: "products/блины"
     });

    Meteor.call("addProduct",{ name: "Блины_с_маком_орехами_и_шоколадом",
      title: "Блины с маком орехами и шоколадом",
      netto: "200гр",
      description: "Блины с маком орехами и шоколадом",  
      price: "180",
      order: 30,
      category: "pancakes",
      options: ['творог','яблочная начинка','варение'],   
      image: "products/блины",
      largeImage: "products/блины"
     });

    //sandwiches
    Meteor.call("addProduct",{ name: "Сендвич_с_семгой_и_сливочным_сыром",
      title: "Сендвич с семгой и сливочным сыром",
      netto: "200гр",
      description: "Сендвич с семгой и сливочным сыром",  
      price: "200",
      order: 10,
      category: "sandwiches",
      image: "products/сэндвич_инжир",
      largeImage: "products/сэндвич_инжир"
     });

    Meteor.call("addProduct",{ name: "Сендвич_с_инжиром_и_бри",
      title: "Сендвич с инжиром и бри",
      netto: "200гр",
      description: "Сендвич с инжиром и бри",  
      price: "200",
      order: 20,
      category: "sandwiches",
      image: "products/сэндвич_инжир",
      largeImage: "products/сэндвич_инжир"
     });

    //omelettes
    Meteor.call("addProduct",{ name: "Яичница_простая",
      title: "Яичница простая",
      netto: "200гр",
      description: "Яичница простая",  
      price: "120",
      order: 10,
      category: "omelettes",
      options: ['сыр','зелень','буженина'],
      image: "products/яичница_простая",
      largeImage: "products/яичница_простая"
     });

    Meteor.call("addProduct",{ name: "Омлет_простой",
      title: "Омлет простой",
      netto: "200гр",
      description: "Омлет простой",  
      price: "140",
      order: 20,
      category: "omelettes",
      options: ['сыр','красный сладкий перец','острые перцы', 'резаные маслины', 'грибы', 'ветчина'],
      image: "products/яичница_простая",
      largeImage: "products/яичница_простая"
     });

    Meteor.call("addProduct",{ name: "Яичница_на_сыре",
      title: "Яичница на сыре",
      netto: "200гр",
      description: "Яичница на сыре",  
      price: "140",
      order: 30,
      category: "omelettes",
      image: "products/яичница",
      largeImage: "products/яичница"
     });

    //yogurt
    Meteor.call("addProduct",{ name: "Йогурт",
      title: "Йогурт собственного приготовления",
      netto: "200гр",
      description: "Йогурт собственного приготовления",  
      price: "120",
      order: 10,
      category: "yogurt",
      options: ['ягоды','мюсли','ананасы'],
      image: "products/йогурт",
      largeImage: "products/йогурт"
     });

    Meteor.call("addProduct",{ name: "Творожная_масса",
      title: "Творожная масса",
      netto: "200гр",
      description: "Йогурт собственного приготовления",  
      price: "140",
      order: 20,
      category: "yogurt",
      options: ['банан','орехи','мюсли'],
      image: "products/йогурт",
      largeImage: "products/йогурт"
     });

  },  


});

// Meteor.publish('products', function listsPublic() {
//   return Lists.find({
//     userId: { $exists: false },
//   }, {
//     fields: Lists.publicFields,
//   });
// });

// Meteor.publish('lists.private', function listsPrivate() {
//   if (!this.userId) {
//     return this.ready();
//   }

//   return Lists.find({
//     userId: this.userId,
//   }, {
//     fields: Lists.publicFields,
//   });
// });