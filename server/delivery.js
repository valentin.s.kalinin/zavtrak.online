import { Meteor } from 'meteor/meteor';
// import delivery from './delivery.json';
// import delivery from './delivery_zones.json';

Meteor.methods({

    deliveryGeo()
    {
        var delivery = {
            "type":"FeatureCollection",         
            "features": []      
        }
        var deliveryGeo = DeliveryGeo.find().fetch(); 

        _.each( deliveryGeo,(item)=>delivery.features.push(JSON.parse(item.features)));
        // console.log("#deliveryGeo",delivery);
        return delivery;
    },


    addDeliveryZone(name, index, features)
    {
       
        let staff = Staff.findOne({userId: this.userId});

        if( staff )
        { 
            DeliveryGeo.insert({
                name: name,
                index: index,
                features: features
            });
        } 
        
    },
    updateDeliveryZone(id, name, index, features)
    {

        let staff = Staff.findOne({userId: this.userId});

        if( staff )
        { 
            DeliveryGeo.update({_id: id},
            {
                name: name,
                index: index,
                features: features
            });
        }         
    },
    _testZone(index)
    {
        console.log(DeliveryGeo.find().fetch());
        console.log(DeliveryGeo.findOne({index:index}));
    },
    removeDeliveryZone(id)
    {            
        let staff = Staff.findOne({userId: this.userId});

        if( staff )
        { 
            DeliveryGeo.remove({_id: id});
        } 
    },

    setDeliveryZone(userId, id)
    {
        let staff = Staff.findOne({userId: this.userId});
        let zone = DeliveryGeo.findOne({_id: id});
        if( staff && zone)
        { 
            UserDelivery.insert({userId: userId, deliveryZoneId: id});
            console.log("update userId", userId)
            Meteor.users.update({_id: userId }, 
            {
                $set: { 'profile.deliveryZone' : zone }
            });
        }
    },
    clearDeliveryZone(userId)
    {
        let staff = Staff.findOne({userId: this.userId});
        if( staff )
        { 
            UserDelivery.remove({userId: userId})
            Meteor.users.update({_id: userId }, 
                {
                    $set: { 'profile.deliveryZone' : '' }
                });
        }
    }
});